ZotCode - Flight Companion App
===============================

# Developers #

* Joshua Crompton - C3165877
* Leigh Cooper - C3088848
* Nathan Davidson - C3187164
* Simon Hartcher - C3185790
* Tyler Haigh - C3182929
* Christian Lassen - C3121707

# Abstract #

This project aims to implement a mobile companion application to the FlightPub flight booking website as part of SENG3160 - Software Project Part 2 at the University of Newcastle (Callaghan), 2015.

FlightPub is an Australian start-up company that specialises in direct ticket sales for airlines. FlightPub does not operate with retail outlets and their vision is to conduct their business via an e-commerce store; they have approached ZotCode to design and implement the solution. Preliminary research has indicated that existing e-commerce flight booking sites contain a significant portion of the required functionality for FlightPub’s website. In addition to this, FlightPub have requested that additional functionality be included in their system to provide a superior product, service, and improve their market share. ZotCode’s goal and project purpose will be to establish FlightPub’s online presence.

# Dependencies #

This project is built using the [Ionic Framework](http://ionicframework.com/). To install the dependencies to build and run our application, we recommend the use of [scoop](http://scoop.sh/) (for Windows machines) or [Homebrew](http://brew.sh/) (for Apple machines). Both of these are freely available command line tools:

1. Download [NodeJS](https://nodejs.org/en/) and the Node Package Manager (NPM) by running `scoop install nodejs` or `brew install node`. NPM is automatically installed with NodeJS. If you have a Unix operating system, you may prefix this with `sudo`
2. Install the following NPM Packages
    a. Gulp - `npm install --global gulp`
    b. Bower - `npm install --global bower`
    c. Cordova and Ionic - `npm install --global cordova ionic`

## Dependencies for Testing ##

Our testing framework [Karma](http://karma-runner.github.io/0.13/index.html) has been set up using [this tutorial](http://mcgivery.com/unit-testing-ionic-app/). To install karma:

1. Run `npm install karma karma-jasmine karma-phantomjs-launcher`
2. Run `npm install -g karma-cli`
3. Run `bower install angular-mocks`

A configuration file for the Karma tests is located in `.\tests\karma.conf.js`.

# To Run #

1. Clone our project `git clone git@bitbucket.org:zotcode/zotcode-mobile.git`
2. Download all of the dependencies listed above
3. Restore all Bower packages by running `bower install` from the project directory
4. Restore all NodeJS packages by running `npm install` from the project directory
5. Compile the JavaScript Sources by running `gulp scripts`. This should generate an `all.js` file in the `www/js/` directory
6. Simulate the application by running `ionic serve`

## To Run Tests ##

A Gulp task has been set up for ease of running tests. Simply run `gulp test` to start Karma using out configuration file

# Connecting to the API #

The API URL details are configured in `jssrc/app-constants.js`. By default, this is set to `.constant("zotCodeApiUrl", "https://localhost:44300/")`. If the Web API is not hosted on this address or port number, feel free to change it. This will require you to run `gulp scripts` to rebuild the `all.js` file

# Compiling to Android #

Installing the [Android SDK](http://developer.android.com/sdk/index.html) is required as per [the Ionic guide's Installation page](http://ionicframework.com/docs/guide/installation.html).

Cordova requires the `ANDROID_HOME` environment variable to be set. This should point to the `[ANDROID_SDK_DIR]\android-sdk` directory (for example `c:\android\android-sdk`).

Next, update your `PATH` to include the `tools/` and `platform-tools/` folder in that folder. So, using `ANDROID_HOME`, you would add both `%ANDROID_HOME%\tools` and `%ANDROID_HOME%\platform-tools`.

In the project directory, run `ionic platform add android` to add the Android build platform

A Makefile has been provided for ease of compiling to Android and can be run with `make all` from the project directory. The Makefile follows the set of commands listed on [the Ionic guide's Publishing page](http://ionicframework.com/docs/guide/publishing.html)

# Compiling to iOS #

Compiling to iOS is not currently supported, however it a guide for compiling to iOS can be found on [the Ionic guide's Installation page](http://ionicframework.com/docs/guide/installation.html)