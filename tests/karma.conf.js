// Karma configuration
// Generated on Wed Sep 23 2015 13:31:15 GMT+1000 (AUS Eastern Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      '../www/lib/angular/angular.js',
      '../www/js/all.js',
      '../www/lib/angular-mocks/angular-mocks.js',
      '../www/lib/angular-ui-router/release/angular-ui-router.js',
      '../www/lib/ionic/js/ionic.js',
      '**/*tests.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: { '../www/js/all.js': ['coverage'] },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],

    coverageReporter: {
      type : 'html',
      dir : '../Reports/KarmaCoverage/'
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
    //logLevel: config.LOG_DEBUG,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],
    //browsers: ['Chrome'], // Requires npm install karma-chrome-launcher --save-dev
    //browsers: ['PhantomJS', 'Chrome'],
    //browsers: ['PhantomJS_custom'],


    // The following is taken from https://github.com/seeq12/karma-phantomjs-launcher/tree/debugger
    // It is an extension to the PhantomJS browser that enables
    // debugging in a web browser

    // you can define custom flags
    customLaunchers: {
      'PhantomJS_custom': {
        base: 'PhantomJS',
        options: {
          windowName: 'my-window',
          settings: {
            webSecurityEnabled: false
          },
        },
        flags: ['--load-images=true'],
        debug: true
      }
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,
    
    // Set the timeout for when running long tests
    // For example, one og the GUID tests is to generate 1000
    // GUIDs and perform an n^2 comparison. This takes a while
    browserNoActivityTimeout: 1000 * 100 // 100 Seconds
  })
}
