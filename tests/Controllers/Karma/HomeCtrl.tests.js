
describe('Home Controller Tests', function() {
	var scope;
	var ctrl;
	var state;
	
	// Services
	var _ApiService;
	var _TokenService;
	var _FlightService;
	var _ChecklistService;
	var _UserService;
	var _q;
	var deferred;
	var accessToken = "mtE9x5YOI5ENAh_hQXFweEHzls2hdTdDvRsza653equQ37Ao8I34lu6DqaIL3e8Q01iZ9Nn4wHCiHUi4rWbCDz-k5wuWuUzyuUoP1oDAKicB8L4OMbhbtNgT2NV_-CyZ3VEE5STPwf3Km6a4licx5b4p9cTqaXhfN2SYGK79cYfZAVnt6f0rwCeum_vpQriM-BeoFuUZFYU4P79vTJOq-GbYdQF10hlyq6LRjZ2_EnLyFhjvPb2W7arPzCIwk4b6DIWu-FOZDkCUR9urvFyHuuTyUZZa2jjnKGXwQVKhdEC6bEjzklDNmBZVP06w-T8A5UQx4Rdd9bwiFIhajUbhS3o2GdktaRe0a1oWzu137bMTVADEL_9GhVIIB_KMAWeQCkplZzfGTpuuOE1cLm0--oa-WXlFv_E2EQ5ldaJSnowkN0nzZhXGA-oTVWYCrvYn6Bx5SIYLqRLmq-QjFqEm_Q";
	
	beforeEach(module('ui.router'));
	beforeEach(module('FlightPub.services'));
	beforeEach(module('FlightPub.controllers'));
	beforeEach(module('FlightPub-Constants'));
	beforeEach(inject(function($rootScope, $controller, $state, $injector, $http,
				ApiService, TokenService, FlightService, ChecklistService,
				UserService, $q, zotCodeApiUrl) {
		
		// Services
		_ApiService       = $injector.get('ApiService');
		_TokenService     = $injector.get('TokenService');
		_FlightService    = $injector.get('FlightService');
		_ChecklistService = $injector.get('ChecklistService');
		_UserService      = $injector.get('UserService');
		_q = $q;
		deferred = $q.defer();
					
		// Nav ctrl
		scope = $rootScope.$new();
		state = $state;
		ctrl = $controller('HomeCtrl',
		{
			$scope: scope,
			$state: state,
			ApiService: _ApiService,
			TokenService: _TokenService,
			FlightService: _FlightService,
			ChecklistService: _ChecklistService,
			UserService: _UserService
		});
		
		// Clear local storage so it doesn't affect other tests
		localStorage.clear();
	}));
	
	
	it ('Can get the Home Controller Tabs', inject(function() {
		
		// Define the expected links
		var expectedTabs = [
			{title: "Dashboard", icon: "ion-home", link: "home.dashboard", tabName: "dashboard"},
			{title: "Checklist", icon: "ion-android-checkbox-outline", link: "home.checklist", tabName: "checklist"},
		];
		
		var actualLinks = scope.tabs;
		
		expect(actualLinks.length).toEqual(2);
		
		for(var i = 0; i < 2; i++) {
			expect(actualLinks[i]).toEqual(expectedTabs[i]);
		}
	}));
	
	it ('Successful user validation downloads token', inject(function() {
		
		// Make a flight and store it
		scope.loginVM.email = "swaggins@hotmail.com";
		scope.loginVM.password = "123456aA@";
		
		var result = {
			data:
			{
				"access_token": accessToken,
				"token_type": "bearer",
				"expires_in": 5183999,
				"userName": "swaggins@hotmail.com",
				
				// In real implementations, these string properies
				// are previxed with a "."
				// I'm removing this so I can run the test
				"issued": new Date(), //"Sun, 04 Oct 2015 03:25:35 GMT",
				"expires": new Date(), //"Thu, 03 Dec 2015 03:25:35 GMT"
			}
		};
		
		// Mock the Api Service call because $q doesn't work
		spyOn(_ApiService, 'post').and.returnValue(deferred.promise);
		deferred.resolve(result);
		scope.$apply();
		
		// Override the $state.go call because it will fail and make the test fail
		// Yay for angular!
		spyOn(state, 'go').and.callFake(function() {
			state.name = "home.dashboard";
		});
		scope.$apply();
		
		// Kill any API Get Requests for now. Not testing that here
		spyOn(_ApiService, 'apiGet').and.callFake(function() {
			// Laugh at your opponent and pass go and collect $200
			console.info("Overiding ApiService GET request. Will there be cake?");
			var def = _q.defer();
			def.reject("The Cake is a Lie");
			return def.promise;
		});
		
		//debugger;
		scope.login();
		scope.$apply();
		
		//debugger;
		var actualToken = _TokenService.all();
		expect(actualToken).toBeDefined();
		expect(actualToken).not.toEqual(null);
		
		// Change the datatypes of the datetimes. BUG HERE
		actualToken.issued = new Date(actualToken.issued);
		actualToken.expires = new Date(actualToken.expires);
		
		expect(actualToken).toEqual(result.data);
		expect(state.name).toEqual("home.dashboard");
	}));
	
	
	it ('Failed user validation displays errors on login page', inject(function() {
		
		// Make a flight and store it
		scope.loginVM.email = "swaggins@hotmail.com";
		scope.loginVM.password = "invalid";
		
		// Mocking functionality for ApiService Post Call
		// Didn't work. Might have needed to use scope.$apply
		// Other way of doing test works fine.
		//
		// spyOn(_ApiService, 'post').and.callFake(function() {
		// 	var deferred = _q.defer();
		// 	deferred.reject("I can't let you do that dave");
		// 	return deferred.promise;
		// });
		
		
		// Generate HAL9000 Error Message
		// 2001: A Space Odyssey
		// https://www.youtube.com/watch?v=ARJ8cAGm6JE
		var HAL9000Error = {
			data: {
				error_description: "I'm sorry Dave, I'm afraid I can't do that"
			}
		};
		
		spyOn(_ApiService, 'post').and.returnValue(deferred.promise);
		deferred.reject(HAL9000Error);
		scope.$apply();
		
		
		//debugger;
		scope.login();
		
		// This is the guy you need to do this!!!!!!!!!!
		scope.$apply();
		
		expect(_ApiService.post).toHaveBeenCalled();
		
		// I have you now Dave ... * shoots laser * *pew pew *
		// https://www.youtube.com/watch?v=JF5jyuX6Gyg
		// http://www.krowmark.com/wp/wp-content/uploads/2015/05/Darth-Vader-darth-vader-18734783-300-355.jpg
		
/*
									Darth Vader Approves - Ascii Art by http://picascii.com/		
                       `     `  `  ` `````` ``````````````````.,:::::::;;:``````````````````````````
                                      `````````````````````.,::;'++++''''+'..```````````````````````
                                      ```````````````````.::;'++++++++++:++';;,`````````````````````
                                      ``````````````````,:;'++++++++++++++'+.'';,```````````````````
                                           ```````````.:;;'++++'###+++`''+;+;++';:``````````````````
                                           ```````````:;;'+++##+###+:++++++++++++': ````````````````
                                       `      ```````::;+++++######+++###++##++++';, ```````````````
                                       `      ``````,:;'++++###+###:#+###+###'++++';.`````````````` 
                                               ``.,:;;++++:++++'+###++#######`###+++;   ````````````
                                                `.,'''++;+';'++'####'#####+##.####++:   ````````````
                                                `,:''++:';;;+++#####+###+####`####++;   ````````````
                                                `,;'+';;:;'++'##########+####`###+++'  `````````````
                                              ```,;';;::;'++#+++++'++##+#####,++#+++'` `````````````
                                              ```,':;:::'+##;#@@#@#####+#####+###++++; `````````````
                                              ` .,:::,;'+++#@#@#:+####+###+##+#####++' `````````````
                                              ` .,,,,:;+#+@###'#@#########'##########+. ````````````
                                              ``.,,:,;++#@@##+@##+`.,,:+#+,;++'+######+ ````````````
                                             ```.,,,:'++@#######+,`,:. .;';++##@#######+ ```````````
                                             ``.,,::;++@@#######..`:,  `;',;;+##########.```````````
                                             ``.,:::'+@@@#######'.`';..;;':;'+#@######### ``````````
                                             `..,::;+#@@@@#####++,:++',:;':;'+'##########```````````
                                             `.,,::'+#@#@######++,;':';,'':+++'##########'``````````
                                             ..,:,;+##@@@#####+'::'';;;:;+##;#+###########``````````
                                            `..,:,'+###@###########+'++++###;;+@##########``````````
                                            `.,,,:+######@########+''#########;;''########``````````
                                            ..,:,;+##@##########'';::,,,'##++#'',,######## `````````
                                           `..,,,'###########'+#';;;;;,..;'#;+#''######### `````````
                                           ..,,,;+############''';;;;':;+++;::'+#++####### `````````
                                        ```.,,,:'##########+###;+''';+#####;::'+##########``````````
                                          ..,,,,+###########+###'';'@'###'##''#'##########.```````  
                                ;`       `.,,,,,#############'###;'#+###;@################:`````    
                              `;''       ..,,,,######@#######+'##'#+###+########;#########;`````    
                              `';+.     `.,,,,+######:;+###++#++##'#############+#########+`````    
                              .';';     .,,,:+#######''''++###+++###+++##########'###+###+#`````    
                              `:;+'`  ``,::;;########+''''++#+'+++#++'####+#+####+###++##+#`````    
                              .''++,  `,;'+++########''+''''+'+#+;''+#####+##########+'+++#`````    
                              `''++;      `.:'+#####+''++++++++++++.`+++++#######+'##+''+++ ````    
                              ,;'++'  ` ` ` `` `    ''++########+############+++#++#++';'+# ````    
                              ;'++#+ `       ``` ` +''++##########################';`+';;;;`````    
                            ``'++#++ `      ```` `#;'''++#########################+,:..`````````    
                             ;'++#+;       ``````#++:;;''++#############+++++#+'#```` ``````````    
                            .'++##+.  ` `  `.,,`#+;+;:;;;'++############+#+##+#+:```````````````    
                           .'+++##+ ` `;+++++++++#;'+';:;'++####################+'` ````````````    
                           ''+++##+ ;''''+++++++++++'';::'++#############+##;:@##+;;: ``````````    
                          :'''++++++'''''++++++++++++#';:;'++##############;:,+####+';`````````     
                          ;''''''++;+'''';+++++++++++##;:,'+++############++,`;###.##+':```````     
                       ` :;''''''''+++++'';+++++++++++++;,`:'++++##########;, ####,:;;+#+,```       
                      ``,;:;''+'''''+'+++++:++++++++++++##..`:++++#########'` ###+.:;;:': '```      
                       ,:;;;''''''+++'+++++:+++++++#######+'`..:'+++++#####'.####+`;;;;''; :``      
                     `.,:;;;;;;'++++++++##+;++++++#####+++##@#:..:'''++++#+++###++ '';;++' ` ``     
                    `.,,::;;;;;''++++++#####++##+++++#+#####+###++''+++++#+####+## ''';:+'`::` `    
                    ,:::;;'''++++++++++++#'+++++##+####@@########+###@############ +''';#'.;:::,    
                   ;;::;'''''''+++++++'+';:#++++##+###############+;+++'++.####### +++';++:;;;;,,   
                ``:;::'';;;''''+++++++'+++',++++++####+#'##########,''''++'####### ++++;##'''';;`.  
               `,:';''';';;;;::'+++++++++++'+#++#######;#:#########,.';'++'#######`++++'###+''''+`` 
               ::;';;''''';;;'+####+##+++;'++++#@@###+##:@#########;.';;'+'#######.+++#+###+++++#;.`
             `:;:'';''';;''''+#######++####++#@@@@##++++';;########+.'';'+'#######,##+#+###+++++#+..
          ` :::';''';;'''++';;'+++++#+++'++##@###@##+++++;#+########.:';'+'#######,++##+###+++++##+ 
        ` .:::;;'''''++';'';;'++#####++++:@#######@##++++',;########'.';''+#######:+++++####+++####'
      `,:;:,;;:';'++'''+++'''++########'++########@##++++';++######+#`''':+#######:#++++####+######'
   `.;:;;;,':'''''''+++#++++++#######+#####+#######@#+++++',;########`''':++######:+++++####++#+###+
   `;;::',':'''++''++###++++++############@+##########+++'';++#######:,';;+;######:+++++##++++#++###
*/		
		
		
		expect(scope.loginVM.invalid).toEqual(true);
		expect(scope.loginVM.errors).toBeDefined();
		expect(scope.loginVM.errors.length).toEqual(1);
		expect(scope.loginVM.errors[0]).toEqual("I'm sorry Dave, I'm afraid I can't do that");
	}));
	
	
	it ('Successful user validation downloads flight data', inject(function() {
		
		// Make a flight and store it
		scope.loginVM.email = "swaggins@hotmail.com";
		scope.loginVM.password = "123456aA@";
		
		var tokenResult = {
			data:
			{
				"access_token": accessToken,
				"token_type": "bearer",
				"expires_in": 5183999,
				"userName": "swaggins@hotmail.com",
				
				// In real implementations, these string properies
				// are previxed with a "."
				// I'm removing this so I can run the test
				"issued": new Date(), //"Sun, 04 Oct 2015 03:25:35 GMT",
				"expires": new Date(), //"Thu, 03 Dec 2015 03:25:35 GMT"
			}
		};
		
		// Mock the flight data because this is the only way
		var flightResult = {
			data: _FlightService.generate()
		};
		
		// Mock the Api Service call because $q doesn't work
		deferred.resolve(tokenResult);
		spyOn(_ApiService, 'post').and.returnValue(deferred.promise);
		scope.$apply();
		
		// Override the $state.go call because it will fail and make the test fail
		// Yay for angular!
		spyOn(state, 'go').and.callFake(function() {
			state.name = "home.dashboard";
		});
		scope.$apply();
		
		// Kill any API Get Requests for now. Not testing that here
		spyOn(_ApiService, 'apiGet').and.callFake(function() {
			// Laugh at your opponent and pass go and collect $200
			console.info("Overiding ApiService GET request");
			var def = _q.defer();
			def.reject("I laugh at you");
			return def.promise;
		});
		
		scope.login();
		scope.$apply();
		
		//debugger;
		var actualToken = _TokenService.all();
		expect(actualToken).toBeDefined();
		expect(actualToken).not.toEqual(null);
		
		// Change the datatypes of the datetimes. BUG HERE
		actualToken.issued = new Date(actualToken.issued);
		actualToken.expires = new Date(actualToken.expires);
		
		expect(actualToken).toEqual(tokenResult.data);
		expect(state.name).toEqual("home.dashboard");
	}));
	
	it ('Can get next flight if a flight exists in local storage', inject(function($controller) {
		
		var flights = [_FlightService.generate() ];
		flights[0].id = 123;
		_FlightService.save(flights);
		
		var tasks = [ _ChecklistService.newTask(123) ];
		_ChecklistService.save(tasks)
		scope.$apply();

		// Re-create thhe Home ctrl to refresh it
		ctrl = $controller('HomeCtrl',
		{
			$scope: scope,
			$state: state,
			ApiService: _ApiService,
			TokenService: _TokenService,
			FlightService: _FlightService,
			ChecklistService: _ChecklistService,
			UserService: _UserService
		});
		scope.$apply();
		//debugger;
		
		var expectedFlight = flights[0];
		var actualFlight = scope.nextFlight;
		scope.$apply();
		
		expect(actualFlight).toBeDefined();
		expect(actualFlight).not.toEqual(null);
		
		// Check the flight data
		expect(actualFlight.id).toEqual(expectedFlight.id);
		expect(actualFlight.code).toEqual(expectedFlight.code);
		expect(actualFlight.departureLocation).toEqual(expectedFlight.departureLocation);
		expect(actualFlight.arrivalLocation).toEqual(expectedFlight.arrivalLocation);

		// BUG HERE: Dates are not deserialised properly
		expect(new Date(actualFlight.departureDate)).toEqual(expectedFlight.departureDate);
		expect(new Date(actualFlight.arrivalDate)).toEqual(expectedFlight.arrivalDate);		
		
		var expectedTasks = tasks;
		var actualTasks = scope.nextFlightTasks;
		scope.$apply();
		
		expect(actualTasks).toBeDefined();
		expect(actualTasks).not.toEqual(null);
		expect(actualTasks.length).toEqual(1);
		expect(actualTasks[0]).toEqual(expectedTasks[0]);
	}));
	
	it ('Redirects to dashboard if token exists', inject(function() {
		
		// Save the token
		var token = 123;
		_TokenService.save(token);

		// Initialise the state and that spying on it		
		state.name = "home.dashboard";
		spyOn(state, 'go').and.callFake(function() {
			state.name = "home.dashboard";
		});
		scope.$apply();

		scope.$broadcast('$ionicView.beforeEnter');
		scope.$apply();

		expect(state.name).toEqual("home.dashboard");
	}));
	
	it ('Redirects to login if token does not exist', inject(function() {
		
		// Initialise the state and that spying on it		
		state.name = "home.dashboard";
		spyOn(state, 'go').and.callFake(function() {
			state.name = "home.login";
		});
		scope.$apply();

		scope.$broadcast('$ionicView.beforeEnter');
		scope.$apply();

		expect(state.name).toEqual("home.login");
	}));
	
	
});