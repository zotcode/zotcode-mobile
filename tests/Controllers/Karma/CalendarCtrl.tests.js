describe('Calendar Controller tests', function(){
	var scope;
	var ctrl;
	var state;
	
	// Services
	var _FlightService;
	var _q;
	var deferred;
	
	beforeEach(module('ui.router'));
	beforeEach(module('FlightPub.services'));
	beforeEach(module('FlightPub.controllers'));
	beforeEach(module('FlightPub-Constants'));
	beforeEach(inject(function($rootScope, $state, $controller, $injector, FlightService, $q) {
		
		// Services
		_FlightService    = $injector.get('FlightService');
		_q = $q;
		deferred = $q.defer();
					
		// Nav ctrl
		scope = $rootScope.$new();
		state = $state;
		ctrl = $controller('CalendarCtrl',
		{
			$scope: scope,
			$state: state,
			FlightService: _FlightService,
		});	
		
		// Clear local storage so it doesn't affect other tests
		localStorage.clear();
	}));
		
		
	it('Can get flight data if it exists', inject(function(){
	
		var flights = [_FlightService.generate() ];
		_FlightService.save(flights);
	
		scope.getFlights();
		var actualFlights = scope.flights;
		
		expect(actualFlights.length).toEqual(1);
		
		for(var i = 0; i < 1; i++) {
			var actualFlight = actualFlights[i];
			var expectedFlight = flights[i];
			
			// Check the flight data
			expect(actualFlight.id).toEqual(expectedFlight.id);
			expect(actualFlight.code).toEqual(expectedFlight.code);
			expect(actualFlight.departureLocation).toEqual(expectedFlight.departureLocation);
			expect(actualFlight.arrivalLocation).toEqual(expectedFlight.arrivalLocation);

			// BUG HERE: Dates are not deserialised properly
			expect(new Date(actualFlight.departureDate)).toEqual(expectedFlight.departureDate);
			expect(new Date(actualFlight.arrivalDate)).toEqual(expectedFlight.arrivalDate);			
		}		
	}));
	
	it('Cannot get flight data if none exists', inject(function(){
		scope.getFlights();
		var actualFlights = scope.flights;
		expect(actualFlights).toEqual(null);
	}));
	
});
