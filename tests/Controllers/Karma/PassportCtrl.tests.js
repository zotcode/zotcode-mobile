describe('Navigation Controller Tests', function() {
	var scope;
	var ctrl;
	var state;

	// Services
	var _PassportService;
	var _UserService;
	var _FlightService;
	
	beforeEach(module('FlightPub.services'));
	beforeEach(module('FlightPub.controllers'));
	beforeEach(module('FlightPub-Constants'));
	beforeEach(inject(function($rootScope, $controller, $injector) {
		
		// Clear local storage so it doesn't affect other tests
		// Needs to come first because scope.user (and other)
		// objects will be calculated on instantiation
		localStorage.clear();
		
		// Token Service
		_PassportService = $injector.get('PassportService');
		_UserService = $injector.get('UserService');
		_FlightService = $injector.get('FlightService');
		
		// Nav ctrl
		scope = $rootScope.$new();
		ctrl = $controller('PassportCtrl',
		{
			$scope: scope,
			
			// Technically not required for injection, but I forgot to
			// remove it from the Controller definition, and the test will fail
			// unless these are included
			$state: {},
			$ionicHistory: {},
			
			PassportService: _PassportService
			
		});
		
	}));
	
	it ('Can get the tabs for the controller', inject(function() {
		
		// Define the expected links
		var expectedTabs = [
			{title: "Home", icon: "ion-home", link: "passport.index", tabName: "index"},
			{title: "Details", icon: "ion-android-person", link: "passport.details", tabName: "details"},
			{title: "Countries", icon: "ion-flag", link: "passport.countries", tabName: "countries"},
			{title: "Map", icon: "ion-earth", link: "passport.map", tabName: "map"}
		];
		
		var actualTabs = scope.tabs;
		
		expect(actualTabs.length).toEqual(4);
		
		for(var i = 0; i < 4; i++) {
			expect(actualTabs[i]).toEqual(expectedTabs[i]);
		}
	}));
	
	it ('Can get the user from local storage if one exists', inject(function() {
		
		// Create the mock user and store it
		var mockUser = new User();
		mockUser.userName = "swaggins@hotmail.com";
		mockUser.firstName = "Frodo";
		mockUser.lastName = "Baggins";
		mockUser.dateOfBirth = new Date(2015, 10,4);
		mockUser.postalAddress =
			{
				streetNumber: 123,
				streetName: "Bag End",
				city: "The Shire",
				state: "NSW",
				country: "Australia"
			};
		mockUser.country = "Australia";
		_UserService.save(mockUser);
		
		// Since scope.user will already have been calculated by now
		// (and be null), we fake a reload of the page and recalculate
		// the user
		scope.user = _PassportService.user();
		var actualUser = scope.user;
		
		expect(actualUser).toBeDefined();
		expect(actualUser).not.toEqual(null);
		expect(actualUser).toEqual(mockUser);

	}));
	
	it ('Cannot get user from local storage if none exists', inject(function() {
		var actualUser = scope.user;
		expect(actualUser).toEqual(null);
	}));
	
	
	it ('Can get the user’s flights from local storage if they exist', inject(function() {
		
		// Create the mock user and store it
		var mockFlights = [ _FlightService.generate() ];
		_FlightService.save(mockFlights);
		
		// Since scope.flights will already have been calculated by now
		// (and be null), we fake a reload of the page and recalculate
		// the flights
		scope.flights = _PassportService.flights();
		var actualFlights = scope.flights;
		
		expect(actualFlights).toBeDefined();
		expect(actualFlights).not.toEqual(null);
		expect(actualFlights.length).toEqual(1);
		
		// Check data
		for (var i = 0; i < 1; i++) {
			// Test would fail if comparing flight objects directly
			// as they are retrieved as an Object, not a Flight.
			// Improvement can be made here
			//
			//expect(actualFlights[i]).toEqual(mockFlights[i]);
			
			var actualFlight = actualFlights[i];
			var mockFlight = mockFlights[i];
			
			expect(actualFlight.id).toEqual(mockFlight.id);
			expect(actualFlight.code).toEqual(mockFlight.code);
			//expect(actualFlight.departureDate).toEqual(mockFlight.departureDate); // BUG HERE
			expect(actualFlight.departureLocation).toEqual(mockFlight.departureLocation);
			//expect(actualFlight.arrivalDate).toEqual(mockFlight.arrivalDate); // BUG HERE
			expect(actualFlight.arrivalLocation).toEqual(mockFlight.arrivalLocation);
			
			// all() method does not convert serialised dates back to Date objects
			expect(new Date(actualFlight.departureDate)).toEqual(mockFlight.departureDate);
			expect(new Date(actualFlight.arrivalDate)).toEqual(mockFlight.arrivalDate);
		}

	}));
	
	it ('Cannot get the user’s flights from local storage if none exist', inject(function() {
		var actualFlights = scope.flights;
		expect(actualFlights).toEqual(null);
	}));
	
	
	it ('Can get the Country Localities from flight data is flights exist in local storage', inject(function() {
		
		// Create the mock user and store it
		var mockFlights = [
			_FlightService.generate(),
			_FlightService.generate()
		];
		mockFlights[0].arrivalLocation = "Spain";
		mockFlights[1].arrivalLocation = "Spain";
		_FlightService.save(mockFlights);
		
		// Since scope.flights will already have been calculated by now
		// (and be null), we fake a reload of the page and recalculate
		// the flights
		scope.countries = _PassportService.countries();
		var actualCountries = scope.countries;
		
		// Check existence of Flights
		expect(actualCountries).toBeDefined();
		expect(actualCountries).not.toEqual(null);
		expect(actualCountries.length).toEqual(1);
		
		// Check data
		expect(actualCountries[0]).toEqual("Spain");
	}));

	it ('Cannot get Country Localities from flights if no flights exist in local storage', inject(function() {
		var actualCountries = scope.countries;
		expect(actualCountries).toBeDefined();
		expect(actualCountries).not.toEqual(null);
		expect(actualCountries.length).toEqual(0);
	}));
	
});
