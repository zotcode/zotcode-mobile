describe('Navigation Controller Tests', function() {
	var scope;
	var ctrl;
	var state;
	var _TokenService;
	
	// Mock the $ionicSideMenuDelegate because that's what is expected.
	// We only need to test that the delegate is called and that it does
	// what we expect it to do
	var mockDelegate =
	{
		active: false,
		toggleLeft: function(){ this.active = !this.active; }
	};
	
	
	beforeEach(module('ui.router'));
	beforeEach(module('FlightPub.services'));
	beforeEach(module('FlightPub.controllers'));
	beforeEach(inject(function($rootScope, $controller, $state, $injector) {
		
		// Reset the mock delegate for $ionicSideMenuDelegate
		mockDelegate = {
			active: false,
			toggleLeft: function(){ this.active = !this.active; }
		};
		
		// Token Service
		_TokenService = $injector.get('TokenService');
		
		// Nav ctrl
		scope = $rootScope.$new();
		state = $state;
		ctrl = $controller('NavCtrl',
		{
			$scope: scope,
			$state: state,
			$ionicSideMenuDelegate: mockDelegate,
			TokenService: _TokenService 
		});
		
		
		
		// Clear local storage so it doesn't affect other tests
		localStorage.clear();
	}));
	
	it ('Can get the Nav Links', inject(function() {
		
		// Define the expected links
		var expectedLinks = [
			{ name: 'Dashboard', link: 'home.dashboard' },
			{ name: 'My Flights', link: 'flight.index' },
			{ name: 'Passport', link: 'passport.index' },
			{ name: 'My Calendar', link: 'calendar.calendar' },
			{ name: 'Settings', link: 'settings.index' }
		];
		
		var actualLinks = scope.links;
		
		expect(actualLinks.length).toEqual(5);
		
		for(var i = 0; i < 5; i++) {
			expect(actualLinks[i]).toEqual(expectedLinks[i]);
		}
	}));
	
	it ('Can go to valid state', inject(function() {
		var stateName = 'home.dashboard';

		// I can't get Jasmine to test that the state has been updated
		// to the new state. It throws an error on $state.go saying
		// "Could not resolve 'home.dashboard' from state ''".
		// I've replaced the scope's call with a mock function
		// so we can test this.

		// Spy on things.
		// 		Use and.callThrough to actually call the function. (DOES NOT WORK!)
		// 		Use and.callFake to take control and define your own functionality
		
		
		/**
		 * 21 October 2015
		 * Now that we know that testing Angular inherently expects us
		 * to mock everything and not actually test the real underlying
		 * implementations, this is fine.
		 * 
		 * In essence, you assume that the implementation of some external API is
		 * correct, and has been thoroughly tested in isolation, so you mock
		 * the implementation and test it based on what you expect/want it to do 
		 */
		
		// Mock the state.go method so we can actually test something
		spyOn(state, 'go').and.callFake(function() {
			state.name = stateName;
		});
		
		try {
			scope.goto(stateName);
			
			// Before realising that mocking functionality was required for testing,
			// the following conversation was had...
			//
			// https://www.youtube.com/watch?v=eg8-dii9Ma0 - Pulp Fiction (1994)
	
			// What does state.name look like?
			// Angular: ... What?
			// *Flips Table* What country you from?
			// Angular: ... What?
			// What ain't no country I ever heard of. They speak English in What?
			// Angular: ... What?
			// English! Do you speak it!
			// Angular: ... Yes
			// Then you know what I'm saying!
			// Angular: ... Yes
			// Describe what state.name looks like!
			// Angular: ... What?
			// Say what again! I dare you, I double dare you! Say what one more time!
			// Angular: ... What? *Insert troll face*
			
			expect(state.go).toHaveBeenCalledWith(stateName);
			expect(state.name).toEqual(stateName);
		} catch (e) {
			// If you want to try calling $state.go with the callThrough spy
			// active, you will get the following error message.
			// Unless you can figure out how to get Karma / PhantomJS to
			// set up the ui-router $state object correctly so it plays nice
			
			expect(e.message).toEqual("Could not resolve 'home.dashboard' from state ''");
			console.error("Karma failed to call $state.go because it couldn't resolve state");
		}
	}));
	
	it ('Cannot go to invalid state', inject(function() {
		var stateName = 'home.dashboard';
		
		// Don't mock the state.go method.
		// In essence, this fails in the same way as it normally would
		// (being that the state is invalid), however it is the fact
		// that the state is invalid to begin with that forces this to fail
		spyOn(state, 'go').and.callThrough();

		try {
			scope.goto(stateName);
			expect(state.go).toHaveBeenCalledWith(stateName);
			expect(state.name).not.toEqual(stateName);
		} catch (e) {
			// If you want to try calling $state.go with the callThrough spy
			// active, you will get the following error message.
			// Unless you can figure out how to get Karma / PhantomJS to
			// set up the ui-router $state object correctly so it plays nice
			
			expect(e.message).toEqual("Could not resolve 'home.dashboard' from state ''");
			console.info("Test Pass: Karma failed to call $state.go because it couldn't resolve state name");
		}
	}));
	
	it ('Can toggle side menu on and off', inject(function() {
		
		// Spy on the mock delegate so we can verify that we have successfully
		// hijacked the $ionicSideMenuDelegate and instead injected our mock
		// delegate into the Controller
		spyOn(mockDelegate, 'toggleLeft').and.callThrough();

		scope.toggleMenu();
		
		// Normally we would call
		// expect(scope.$ionicSideMenuDelegate.toggleLeft).toHaveBeenCalled();
		// however, since we mocked $ionicSideMenuDelegate, we need to check
		// that the mock instance was called and that its state has been changed
		expect(mockDelegate.toggleLeft).toHaveBeenCalled();
		expect(mockDelegate.active).toEqual(true);
		
		scope.toggleMenu();
		
		expect(mockDelegate.toggleLeft).toHaveBeenCalled(); // May be redundant as it has been called once already
		expect(mockDelegate.active).toEqual(false);
	}));
	
	it ('Can verify logged in user if given a valid token', inject(function() {
		
		// Create the token and store it in local storage
		var token = {
			access_token: "ABC123",
			token_type: "bearer",
			expires_in: 5183999,
			userName: "swaggins@hotmail.com",
			issued: new Date(),
			expires: new Date() 
		};
		_TokenService.save(token);
		
		var result =  scope.loggedIn();
		expect(result).toEqual(true);
	}));
	
	
	it ('Treats the user as not logged in if there is no valid token', inject(function() {
		
		// Do not create the token and store it...
		
		var result =  scope.loggedIn();
		expect(result).toEqual(false);
	}));

});
