
describe('Home Controller Tests', function() {
	var scope;
	var ctrl;
	
	// Services
	var _GeoCodeService;
	var _PassportService;
	var _FlightService;
	var _q;
	var deferred;
	
	var tokyoLocalityData = {
		results:[
      {
         "address_components" : [
            {
               "long_name" : "Tokyo",
               "short_name" : "Tokyo",
               "types" : [ "colloquial_area", "locality", "political" ]
            },
            {
               "long_name" : "Japan",
               "short_name" : "JP",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Tokyo, Japan",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 35.8175168,
                  "lng" : 139.9198565
               },
               "southwest" : {
                  "lat" : 35.5208632,
                  "lng" : 139.5629047
               }
            },
            "location" : {
               "lat" : 35.7090259,
               "lng" : 139.7319925
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 35.8175168,
                  "lng" : 139.9198565
               },
               "southwest" : {
                  "lat" : 35.5208632,
                  "lng" : 139.5629047
               }
            }
         },
         "place_id" : "ChIJXSModoWLGGARILWiCfeu2M0",
         "types" : [ "colloquial_area", "locality", "political" ]
      }]};

	beforeEach(module('FlightPub.services'));
	beforeEach(module('FlightPub.controllers'));
	beforeEach(module('FlightPub-Constants'));
	beforeEach(inject(function($rootScope, $controller, $injector,
				GeoCodeService, PassportService, $q, zotCodeApiUrl) {
		
		// Services
		_GeoCodeService   = $injector.get('GeoCodeService');
		_PassportService  = $injector.get('PassportService');
		_FlightService    = $injector.get('FlightService');
		_q = $q;
		deferred = $q.defer();
					
		// Map ctrl
		scope = $rootScope.$new();
		ctrl = $controller('MapCtrl',
		{
			$scope: scope,
			GeoCodeService: _GeoCodeService,
			PassportService: _PassportService,
		});
		
		// Clear local storage so it doesn't affect other tests
		localStorage.clear();
	}));
	
	
	it ('Can get the Map Object', inject(function() {
		var expected = {
			center: { latitude: 45, longitude: -73 },
			zoom: 1,
			dragging: true
		};
		
		var actual = scope.map ;
		
		expect(actual).toBeDefined();
		expect(actual).not.toEqual(null);
		expect(actual).toEqual(expected);
	}));
	
	it ('Can get the Map Markers when no data in local storage', inject(function() {
		var expected = [];
		var actual = scope.markers;
		
		expect(actual).toBeDefined();
		expect(actual).not.toEqual(null);
		expect(actual).toEqual(expected);
	}));
	
	it ('Can get the Map Markers when data is in local storage', inject(function($rootScope, $controller) {
		
		var flights = [_FlightService.generate() ];
		_FlightService.save(flights);

		// Prevent $httpBackend Request by mocking call
		deferred.resolve(tokyoLocalityData);
		spyOn(_GeoCodeService, 'localityLatLong').and.returnValue(deferred.promise);
		
		scope.$apply();
		//debugger;
		
		var expected = [];
		var actual = scope.markers;
		scope.$apply();
		
		// Re-create thhe map ctrl to refresh it
		scope = $rootScope.$new();
		ctrl = $controller('MapCtrl',
		{
			$scope: scope,
			GeoCodeService: _GeoCodeService,
			PassportService: _PassportService,
		});
		scope.$apply();
		
		expected = [
			{
				id:0,
				coords: {
					latitude : 35.7090259,
                	longitude: 139.7319925
				}
			}
		];
		actual = scope.markers;
		scope.$apply();
		
		expect(actual).toBeDefined();
		expect(actual).not.toEqual(null);
		expect(actual).toEqual(expected);
	}));
	
});