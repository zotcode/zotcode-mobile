describe('Flight Controller tests', function(){
	var scope;
	var ctrl;
	var state;
	
	// Services
	var _FlightService;
	var _ChecklistService;
	var _NoteService;
	var _q;
	var deferred;
	var accessToken = "mtE9x5YOI5ENAh_hQXFweEHzls2hdTdDvRsza653equQ37Ao8I34lu6DqaIL3e8Q01iZ9Nn4wHCiHUi4rWbCDz-k5wuWuUzyuUoP1oDAKicB8L4OMbhbtNgT2NV_-CyZ3VEE5STPwf3Km6a4licx5b4p9cTqaXhfN2SYGK79cYfZAVnt6f0rwCeum_vpQriM-BeoFuUZFYU4P79vTJOq-GbYdQF10hlyq6LRjZ2_EnLyFhjvPb2W7arPzCIwk4b6DIWu-FOZDkCUR9urvFyHuuTyUZZa2jjnKGXwQVKhdEC6bEjzklDNmBZVP06w-T8A5UQx4Rdd9bwiFIhajUbhS3o2GdktaRe0a1oWzu137bMTVADEL_9GhVIIB_KMAWeQCkplZzfGTpuuOE1cLm0--oa-WXlFv_E2EQ5ldaJSnowkN0nzZhXGA-oTVWYCrvYn6Bx5SIYLqRLmq-QjFqEm_Q";
	
	var mockIonicModal = {
        show: function() {
        },
        hide: function() {
        },
		fromTemplateUrl: function(templateUrl, options){
			var dfd = _q.defer()
			dfd.reject("Herr I'm a Derr");
			return dfd.promise
		},
      };
	  
	var mockIonicHistory = {
		viewHistory: function(){}
	};
	
	beforeEach(module('ui.router'));
	beforeEach(module('FlightPub.services'));
	beforeEach(module('FlightPub.controllers'));
	beforeEach(module('FlightPub-Constants'));
	beforeEach(inject(function($rootScope, $state, $controller, $injector,
				FlightService, ChecklistService,
				NoteService, $q) {
		
		// Services
		_FlightService    = $injector.get('FlightService');
		_ChecklistService = $injector.get('ChecklistService');
		_NoteService 	  = $injector.get('NoteService');
		_q = $q;
		deferred = $q.defer();
					
		// Nav ctrl
		scope = $rootScope.$new();
		state = $state;
		ctrl = $controller('FlightCtrl',
		{
			$scope: scope,
			$state: state,
			FlightService: _FlightService,
			ChecklistService: _ChecklistService,
			NoteService: _NoteService,
			$ionicModal: mockIonicModal,
			$ionicHistory: mockIonicHistory		
			
		});	
		
		// Clear local storage so it doesn't affect other tests
		localStorage.clear();
	}));
		
		
	it('Can get the flight tabs', inject(function(){
	
		var tabs = [
			{title: "Details", icon: "ion-android-plane", link: "flight.details", tabName: "details"},
			{title: "Checklist", icon: "ion-android-checkbox-outline", link: "flight.checklist", tabName: "checklist`"},
			{title: "Notes", icon: "ion-ios-paper-outline", link: "flight.notes", tabName: "notes`"},
			{title: "POI", icon: "ion-arrow-shrink" ,link: "flight.poi", tabName: "poi"},
			{title: "Weather", icon: "ion-ios-partlysunny", link: "flight.weather", tabName: "weather"}
		];
		
		var realTabs = scope.tabs;
		
		expect(realTabs.length).toEqual(5);
		
		for(var i = 0; i < 5; i++) {
			expect(realTabs[i]).toEqual(tabs[i]);
		}		
	}));

	it('Toggles a checklist item', function(){
		var checklistItem = _ChecklistService.newTask(1);		
		scope.toggleChecklistItem(checklistItem);		
		expect(checklistItem.done).toBe(true);
		scope.toggleChecklistItem(checklistItem);		
		expect(checklistItem.done).toBe(false);
	});
	
	it('returns to flight.index', function(){
		spyOn(state, 'go');
		scope.back();
		expect(state.go).toHaveBeenCalled();
		//Index url is blank.
		expect(state.current.name).toEqual('');
	});
	
	it('deletes a task', function(){
		spyOn(_ChecklistService, 'delete');
		scope.flightChecklist = [];
		expect(scope.flightChecklist.length).toEqual(0);
		scope.flightId = 1;		
		scope.newTask();
		//Get the id of the newly created task
		var taskId = scope.flightChecklist[0].id;
		expect(scope.flightChecklist.length).toEqual(1);
		scope.deleteTask(taskId);
		expect(scope.flightChecklist.length).toEqual(0);
	});
	
	it('creates a new note', function(){
		spyOn(_NoteService, 'newNote').and.returnValue(new Note());
		scope.flightId = 1;
		scope.flightNotes = [];		
		expect(scope.flightNotes.length).toEqual(0);
		scope.newNote();
		expect(scope.flightNotes.length).toEqual(1);
		expect(_NoteService.newNote).toHaveBeenCalled();
		
	});
	
	it('deletes a note', function(){
		spyOn(_NoteService, 'delete');
		scope.noteModal = mockIonicModal;
		scope.flightNotes = [{guid: 1}];
		expect(scope.flightNotes.length).toEqual(1);
		scope.deleteNote(1);
		expect(_NoteService.delete).toHaveBeenCalled();
		expect(scope.flightNotes.length).toEqual(0);
	});
	
	it('creates a new task', function(){
		spyOn(_ChecklistService, 'newTask');
		scope.flightChecklist = [];
		scope.newTask();
		expect(_ChecklistService.newTask).toHaveBeenCalled();
		expect(scope.flightChecklist.length).toEqual(1);
		
	});
	
	
	
	
	
	
});