/**
 * The following tests are database dependent. If the tester's database
 * does not contain the valid details as outlined in the tests below,
 * all of the following tests will fail.
 */
describe('API Service Tests', function() {
	var ApiService;
	var accessToken;
	var invalidAccessToken;
	
	beforeEach(module('FlightPub-Constants'));
	beforeEach(module('FlightPub.services'));
	beforeEach(inject(function($injector) {
		ApiService = $injector.get('ApiService');
		
		// Clear local storage so it doesn't affect other tests
    	localStorage.clear();
		
		// This token was generated Sun, 04 Oct 2015 04:07:06 GMT
		// and expires Thu, 03 Dec 2015 04:07:06 GMT
		accessToken = "mtE9x5YOI5ENAh_hQXFweEHzls2hdTdDvRsza653equQ37Ao8I34lu6DqaIL3e8Q01iZ9Nn4wHCiHUi4rWbCDz-k5wuWuUzyuUoP1oDAKicB8L4OMbhbtNgT2NV_-CyZ3VEE5STPwf3Km6a4licx5b4p9cTqaXhfN2SYGK79cYfZAVnt6f0rwCeum_vpQriM-BeoFuUZFYU4P79vTJOq-GbYdQF10hlyq6LRjZ2_EnLyFhjvPb2W7arPzCIwk4b6DIWu-FOZDkCUR9urvFyHuuTyUZZa2jjnKGXwQVKhdEC6bEjzklDNmBZVP06w-T8A5UQx4Rdd9bwiFIhajUbhS3o2GdktaRe0a1oWzu137bMTVADEL_9GhVIIB_KMAWeQCkplZzfGTpuuOE1cLm0--oa-WXlFv_E2EQ5ldaJSnowkN0nzZhXGA-oTVWYCrvYn6Bx5SIYLqRLmq-QjFqEm_Q";
		invalidAccessToken = "ABC123";
	}));

	it ('Can download token with valid details', inject(function(ApiService) {
		// Define the token
		var login = new LoginViewModel();
		login.email = "swaggins@hotmail.com";
		login.password = "123456aA@";
		login.grant_type = "password";
		
		// Define the expected values
		var expectedToken =
		{
			"access_token": "ABC123", // Unable to test
			"token_type": "bearer",
			"expires_in": 5183999,
			"userName": "swaggins@hotmail.com",
			".issued": new Date(), //"Sun, 04 Oct 2015 03:25:35 GMT",
			".expires": new Date(), //"Thu, 03 Dec 2015 03:25:35 GMT"
		}

		// Create the actual token		
		ApiService.post("token", login).then(function(actualToken) {
			expect(actualToken).toBeDefined();
			expect(actualToken).not.toEqual(null);
			actualToken = actualToken.data;
			
			// Verify the received data
			expect(actualToken.access_token).not.toEqual(null); // Unable to test
			expect(actualToken.token_type).toEqual(expectedToken.token_type);
			expect(actualToken.expires_in).toEqual(expectedToken.expires_in);
			expect(new Date(actualToken.issued)).toEqual(expectedToken.issued);
			expect(new Date(actualToken.expires)).toEqual(expectedToken.expires);
		}, function(error) {
			expect(error).toBeUndefined();
		});
	}));
	
	it ('Cannot download token for invalid details', inject(function(ApiService) {
		// Define the token
		var login = new LoginViewModel();
		login.email = "swaggins@hotmail.com";
		login.password = "invalid";
		login.grant_type = "password";
		
		// Define the expected values
		var expectedToken =
		{
			"error": "invalid_grant",
			"error_description": "The user name or password is incorrect."
		}

		// Create the actual token		
		ApiService.post("token", login).then(function(actualToken) {
			expect(actualToken).toBeDefined();
			expect(actualToken).not.toEqual(null);
			actualToken = actualToken.data;
			
			// Verify the received data
			expect(actualToken).toEqual(expectedToken);
		}, function(error) {
			expect(error).toBeUndefined();
			expect(false).toEqual(true);
		});
	}));
	
	it ('Can get all flights for user with valid token', inject(function(ApiService) {
		// Define the expected values
		var expectedFlights = [
		{
			"id": 1,
			"code": "AA1735",
			"departureDate": "2015-09-23T09:50:00",
			"departureLocation": "Atlanta",
			"arrivalDate": "2015-09-24T09:00:00",
			"arrivalLocation": "Rio De Janeiro",
			"arriveStopOverDate": "2015-09-23T11:50:00",
			"departStopOverDate": "2015-09-23T23:20:00"
		}];
		
		// Create the actual token		
		ApiService.apiGet("flights", accessToken).then(function(actualFlights) {
			expect(actualFlights).toBeDefined();
			expect(actualFlights).not.toEqual(null);
			actualFlights = actualFlights.data;
			expect(actualFlights.length).toEqual(1);
			
			// Verify the received data
			for (var i = 0; i < 1; i++) {
				var actualFlight = actualFlights[i];
				var expectedFlight = expectedFlights[i];
				
				expect(actualFlight).toEqual(expectedFlight);
			}
		}, function(error) {
			expect(error).toBeUndefined();
			expect(false).toEqual(true);
		});
	}));
	
	it ('Cannot get all flights for user without valid token', inject(function(ApiService) {
		// Define the expected values
		var expectedError =
		{
			"message": "Authorization has been denied for this request."
		};
		
		// Create the actual token		
		ApiService.apiGet("flights", invalidAccessToken).then(function(actualError) {
			expect(actualError).toBeDefined();
			expect(actualError).not.toEqual(null);
			actualError = actualError.data;
			expect(actualError).toEqual(expectedError);
		}, function(error) {
			expect(error).toBeUndefined();
			expect(false).toEqual(true);
		});
	}));
	
	it ('Can get user data for user with valid token', inject(function(ApiService) {
		// Define the expected values
		var expectedData =
		{
			"userName": "swaggins@hotmail.com",
			"firstName": "Frodo",
			"lastName": "Baggins",
			"dateOfBirth": "1997-01-01T00:00:00",
			"postalAddress": {
				"streetNumber": 1,
				"streetName": "Bag End",
				"city": "The Shire",
				"state": "Middle Earth",
				"country": "Australia"
			}
		};
		
		// Create the actual token		
		ApiService.apiGet("userinfo", accessToken).then(function(actualData) {
			expect(actualData).toBeDefined();
			expect(actualData).not.toEqual(null);
			actualData = actualData.data;
			
			expect(actualData).toEqual(expectedData);
		}, function(error) {
			expect(error).toBeUndefined();
			expect(false).toEqual(true);
		});
	}));
	
	it ('Cannot get user data for user without valid token', inject(function(ApiService) {
		// Define the expected values
		var expectedError =
		{
			"message": "Authorization has been denied for this request."
		};
		
		// Create the actual token		
		ApiService.apiGet("userinfo", invalidAccessToken).then(function(actualError) {
			expect(actualError).toBeDefined();
			expect(actualError).not.toEqual(null);
			actualError = actualError.data;
			expect(actualError).toEqual(expectedError);
		}, function(error) {
			expect(error).toBeUndefined();
			expect(false).toEqual(true);
		});
	}));
	
});