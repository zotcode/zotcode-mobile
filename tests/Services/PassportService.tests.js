describe('Note Service Tests', function() {
	var PassportService;
	var FlightService;
	
	beforeEach(module('FlightPub-Constants'));
	beforeEach(module('FlightPub.services'));
	beforeEach(inject(function($injector) {
		PassportService = $injector.get('PassportService');
		FlightService = $injector.get('FlightService');
		
		// Clear local storage so it doesn't affect other tests
    	localStorage.clear();
	}));

	it ('Can get the countries of all stored flights', inject(function(PassportService, FlightService) {
		// Save a flight
		var flights = [ FlightService.generate() ];
		FlightService.save(flights);
		
		// Get the countries		
		var countries = PassportService.countries();
		expect(countries).toBeDefined();
		expect(countries).not.toEqual(null);
		expect(countries.length).toEqual(1);
		var country = countries[0];

		// Check the note data
		expect(country).toEqual("Spain");
	}));
	
	it ('Can get the countries of all stored flights', inject(function(PassportService, FlightService) {
		// Attempt tp get the countries		
		var countries = PassportService.countries();
		expect(countries).toBeDefined();
		expect(countries).not.toEqual(null);
		expect(countries.length).toEqual(0);
	}));
	
});