describe('StorageService Service Tests', function() {
  var StorageService;
  
  beforeEach(module('FlightPub.services'));
  beforeEach(inject(function($injector) {
    StorageService = $injector.get('StorageService');
    
    // Clear local storage so it doesn't affect other tests
    localStorage.clear();
  }));

  // Simple save and load methods

  it ('Can save and load simple data', inject(function(StorageService) {
    
    var key = 'Simple';
    var data = 'Simple';
    
    StorageService.save(key, data);
    var savedData = StorageService.load(key);
    
    expect(savedData).toEqual('Simple');
  }));
  
  it ('Cannot save and load array data', inject(function(StorageService) {
    
    var key = 'Array';
    var data = ['Data1', 'Data2', 'Data3'];
    
    StorageService.save(key, data);
    var savedData = StorageService.load(key);
    
    expect(savedData).toEqual('Data1,Data2,Data3');
  }));
  
  it ('Cannot save and load object data', inject(function(StorageService) {
    
    var key = 'Person';
    var data = {fName:"Bob", lName:"McBob"};
    
    StorageService.save(key, data);
    var savedData = StorageService.load(key);
    
    expect(savedData).toEqual("[object Object]");
  }));
  
  // Serialise and Deserialise methods
  
  it ('Can serialise and deserialise simple data', inject(function(StorageService) {
    
    var key = 'Simple';
    var data = 'Simple';
    
    StorageService.serialise(key, data);
    var savedData = StorageService.deserialise(key);
    
    expect(savedData).toEqual('Simple');
  }));
  
  it ('Can serialise and deserialise array data', inject(function(StorageService) {
    
    var key = 'Array';
    var data = ['Data1', 'Data2', 'Data3'];
    
    StorageService.serialise(key, data);
    var savedData = StorageService.deserialise(key);
    
    expect(savedData[0]).toEqual("Data1");
    expect(savedData[1]).toEqual("Data2");
    expect(savedData[2]).toEqual("Data3");
  }));
  
  it ('Can serialise and deserialise object data', inject(function(StorageService) {
    
    var key = 'Person';
    var data = {fName:"Bob", lName:"McBob"};
    
    StorageService.serialise(key, data);
    var savedData = StorageService.deserialise(key);
    
    expect(savedData.fName).toEqual("Bob");
    expect(savedData.lName).toEqual("McBob");
  }));
  
  // Other tests
  
  it ('Cannot retrieve something that is not saved', inject(function(StorageService) {
    
    var key = 'Person';
    var invalidKey = 'Bob';
    var data = {fName:"Bob", lName:"McBob"};
    
    StorageService.serialise(key, data);
    var savedData = StorageService.deserialise(invalidKey);
    
    expect(savedData).toEqual(null);
  }));

});
