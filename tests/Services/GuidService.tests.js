// According to this stack overflow post http://stackoverflow.com/a/31361163
// PhantomJS doesn't implement the String.includes method
// The following is the code that implements the includes method
if (!String.prototype.includes) {
  String.prototype.includes = function() {'use strict';
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}

describe('Guid Service Tests', function() {
  var GuidService;
  
  beforeEach(module('FlightPub.services'));
  beforeEach(inject(function($injector) {
    GuidService = $injector.get('GuidService');
  }));

  it ('Can create a GUID', inject(function(GuidService) {
    var guid = GuidService.create();
    
    expect(guid).toBeDefined();
  	expect(guid).not.toEqual(null);
  	expect(guid.includes("4")).toEqual(true);
  }));
  
  it ('Guids are unique', inject(function(GuidService) {
    var guids = new Array();
    
    // Create 1000 Guids
    for (var i = 0; i < 100; i++) {
      var g = GuidService.create();
      guids.push(g);
    }
    
    expect(guids.length).toEqual(100);
    
    // Perform an O(n^2) check against all guids
    // to verify that they are all unique
    for (var i = 0; i < 100; i++) {
        for(var j = 0; j < 100; j++) {
            if (i != j) {
                expect(guids[i]).toBeDefined();
  	            expect(guids[i]).not.toEqual(null);
  	            expect(guids[i].includes("4")).toEqual(true);
          
                expect(guids[i]).not.toEqual(guids[j]);
            }
        }
    }
  }));
});
