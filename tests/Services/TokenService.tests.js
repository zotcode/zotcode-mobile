describe('Token Service Tests', function() {
	var TokenService;
	
	beforeEach(module('FlightPub.services'));
	beforeEach(inject(function($injector) {
		TokenService = $injector.get('TokenService');
		
		// Clear local storage so it doesn't affect other tests
    	localStorage.clear();
	}));
	
	it ('Can save and load a token from local storage', inject(function(TokenService) {
		// Define the expected token
		var expectedToken = {
			access_token: "ABC123",
			token_type: "bearer",
			expires_in: 5183999,
			userName: "swaggins@hotmail.com",
			issued: new Date(),
			expires: new Date() 
		};
		
		// Save and reload the token
		TokenService.save(expectedToken);
		var storedToken = TokenService.all();
		expect(storedToken).toBeDefined();
		expect(storedToken).not.toEqual(null);
		
		// Verify the stored data
		expect(storedToken.access_token).toEqual(expectedToken.access_token);
		expect(storedToken.token_type).toEqual(expectedToken.token_type);
		expect(storedToken.expires_in).toEqual(expectedToken.expires_in);
		expect(storedToken.userName).toEqual(expectedToken.userName);
		
		//expect(storedToken.issued).toEqual(expectedToken.issued); // BUG HERE
		//expect(storedToken.expires).toEqual(expectedToken.expires); // BUG HERE
		
		// Dates are not converted back from their string notation
		expect(new Date(storedToken.issued)).toEqual(expectedToken.issued);
		expect(new Date(storedToken.expires)).toEqual(expectedToken.expires);
	}));
	
	it ('Cannot load a token if none exists in local storage', inject(function(TokenService) {
		var storedToken = TokenService.all();
		expect(storedToken).toEqual(null);
	}));
  
});
