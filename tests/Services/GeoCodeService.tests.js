// According to this stack overflow post http://stackoverflow.com/a/31361163
// PhantomJS doesn't implement the String.includes method
// The following is the code that implements the includes method
if (!String.prototype.includes) {
  String.prototype.includes = function() {'use strict';
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}

describe('Geo Code Service Tests', function() {
  var GeoCodeService;
  
  beforeEach(module('FlightPub-Constants'));
  beforeEach(module('FlightPub.services'));
  beforeEach(inject(function($injector) {
    GeoCodeService = $injector.get('GeoCodeService');
  }));

  it ("Can get a country's data", inject(function(GeoCodeService) {
    var expected = [
      {
         "address_components" : [
            {
               "long_name" : "Spain",
               "short_name" : "ES",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Spain",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 43.7923495,
                  "lng" : 4.3277839
               },
               "southwest" : {
                  "lat" : 27.6378936,
                  "lng" : -18.160788
               }
            },
            "location" : {
               "lat" : 40.46366700000001,
               "lng" : -3.74922
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 45.244,
                  "lng" : 5.098
               },
               "southwest" : {
                  "lat" : 35.17300000000001,
                  "lng" : -12.524
               }
            }
         },
         "place_id" : "ChIJi7xhMnjjQgwR7KNoB5Qs7KY",
         "types" : [ "country", "political" ]
      }];
	  
	  GeoCodeService.countryLatLong('Spain').then(function(actual) {
	      expect(actual.data).toEqual(expected);
    }, function(error) {
        expect(error).toBeUndefined();
    });
  }));
  
  it ("Can get a locality's data", inject(function(GeoCodeService) {
    var expected = [
      {
         "address_components" : [
            {
               "long_name" : "Tokyo",
               "short_name" : "Tokyo",
               "types" : [ "colloquial_area", "locality", "political" ]
            },
            {
               "long_name" : "Japan",
               "short_name" : "JP",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Tokyo, Japan",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 35.8175168,
                  "lng" : 139.9198565
               },
               "southwest" : {
                  "lat" : 35.5208632,
                  "lng" : 139.5629047
               }
            },
            "location" : {
               "lat" : 35.7090259,
               "lng" : 139.7319925
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 35.8175168,
                  "lng" : 139.9198565
               },
               "southwest" : {
                  "lat" : 35.5208632,
                  "lng" : 139.5629047
               }
            }
         },
         "place_id" : "ChIJXSModoWLGGARILWiCfeu2M0",
         "types" : [ "colloquial_area", "locality", "political" ]
      }];
	  
	  GeoCodeService.localityLatLong('Tokyo').then(function(actual) {
	      expect(actual.data).toEqual(expected);
    }, function(error) {
        expect(error).toBeUndefined();
    });
  }));
  
  it ("Can get a country's coords", inject(function(GeoCodeService) {
    var expectedCoords = { "lat" : 40.46366700000001, "long" : -3.74922 };
	  
	  GeoCodeService.countryLatLong('Spain').then(function(actual) {
	      var actualCoords = GeoCodeService.coords(actual.data);
        expect(actualCoords).toEqual(expectedCoords);
    }, function(error) {
        expect(error).toBeUndefined();
    });
  }));
  
  it ("Can get a locality's coords", inject(function(GeoCodeService) {
    var expectedCoords = { "lat" : 35.7090259, "long" : 139.7319925 };
	  
	  GeoCodeService.localityLatLong('Tokyo').then(function(actual) {
	      var actualCoords = GeoCodeService.coords(actual.data);
        expect(actualCoords).toEqual(expectedCoords);
    }, function(error) {
        expect(error).toBeUndefined();
    });
  }));
  
  it ("Cannot get an invalid country’s data", inject(function(GeoCodeService) {
    var expectedData = [];
	  
	  GeoCodeService.countryLatLong('X').then(function(actual) {
	      var actualData = actual.data;
        expect(actualData).toEqual(expectedData);
    }, function(error) {
        expect(error).toBeUndefined();
    });
  }));
  
  it ("Cannot get an invalid locality’s data", inject(function(GeoCodeService) {
    var expectedData = [];
	  
	  GeoCodeService.localityLatLong('X').then(function(actual) {
	      var actualData = actual.data;
        expect(actualData).toEqual(expectedData);
    }, function(error) {
        expect(error).toBeUndefined();
    });
  }));

});
