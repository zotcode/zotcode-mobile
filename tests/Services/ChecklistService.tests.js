// According to this stack overflow post http://stackoverflow.com/a/31361163
// PhantomJS doesn't implement the String.includes method
// The following is the code that implements the includes method
if (!String.prototype.includes) {
  String.prototype.includes = function() {'use strict';
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}

describe('Checklist Service Tests', function() {
	var ChecklistService;
	
	beforeEach(module('FlightPub.services'));
	beforeEach(inject(function($injector) {
		ChecklistService = $injector.get('ChecklistService');
		
		// Clear local storage so it doesn't affect other tests
    	localStorage.clear();
	}));
	
	it ('Can create a Task', inject(function(ChecklistService) {
		var flightNum = 1;
		var now = new Date();
		var expectedDueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 2);
	
		var expectedTask = {
			title: "New Task",
			dueDate: expectedDueDate,
			flightId: flightNum,
			done: false
		};
		var actualTask = ChecklistService.newTask(flightNum);
		
		expect(actualTask).toBeDefined();
		expect(actualTask).not.toEqual(null);
		
		expect(actualTask.title).toEqual(expectedTask.title);
		expect(actualTask.dueDate).toEqual(expectedTask.dueDate);
		expect(actualTask.flightId).toEqual(expectedTask.flightId);
		expect(actualTask.done).toEqual(expectedTask.done);
		
		// ID is a guid. We can only test that it is there
		// and contains 4 (version 4)
		expect(actualTask.id).toBeDefined();
		expect(actualTask.id).not.toEqual(null);
		expect(actualTask.id.includes("4")).toEqual(true);
	}));
  
	it ('Can generate a list of Tasks', inject(function(ChecklistService) {
		var now = new Date();
	
		var passport = {
			title: "Passport",
			dueDate: now,
			flightId: 0,
			id: 0,
			done: false
		};
		var visa = {
			title: "Visa",
			dueDate: now,
			flightId: 0,
			id: 0,
			done: false
		};
		
		var expectedTasks = [passport, visa];
		var actualTasks = ChecklistService.generateTasks();
		
		expect(actualTasks).toBeDefined();
		expect(actualTasks).not.toEqual(null);
		expect(actualTasks.length).toEqual(2);
		
		for(var i = 0; i < 2; i++) {
			expect(actualTasks[i].title).toEqual(expectedTasks[i].title);
			expect(actualTasks[i].dueDate).toEqual(expectedTasks[i].dueDate);
			expect(actualTasks[i].flightId).toEqual(expectedTasks[i].flightId);
			expect(actualTasks[i].id).toEqual(expectedTasks[i].id);
			expect(actualTasks[i].done).toEqual(expectedTasks[i].done);
		}
	}));
  
	it ('Can save and load all tasks from Local Storage', inject(function(ChecklistService) {
	
		// Generate the task
		var flightNum = 1;
		var tasks = [ ChecklistService.newTask(flightNum) ];
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);

		// Save and reload the task
		ChecklistService.save(tasks);
		var allTasks = ChecklistService.all();
		expect(allTasks).toBeDefined();
		expect(allTasks).not.toEqual(null);
		expect(allTasks.length).toEqual(1);
		
		expect(allTasks[0].title).toEqual(tasks[0].title);
		//expect(allTasks[0].dueDate).toEqual(tasks[0].dueDate); // BUG HERE
		expect(allTasks[0].flightId).toEqual(tasks[0].flightId);
		expect(allTasks[0].done).toEqual(tasks[0].done);
		
		// BUG: all() does not parse datetime values back to a datetime object
		var returnDate = new Date(allTasks[0].dueDate)
		expect(returnDate).toEqual(tasks[0].dueDate);
		
		// ID is a guid. We can only test that it is there
		// and contains 4 (version 4)
		expect(allTasks[0].id).toBeDefined();
		expect(allTasks[0].id).not.toEqual(null);
		expect(allTasks[0].id.includes("4")).toEqual(true);
	}));
	
	it ('Can get all tasks for a given flight Id', inject(function(ChecklistService) {
	
		// Generate the task
		var targetFlightNum = 1;
		var otherFlightNum = 1;
		var tasks =
			[
				ChecklistService.newTask(targetFlightNum),
				ChecklistService.newTask(otherFlightNum)
			];
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(2);

		// Save and reload the task
		ChecklistService.save(tasks);
		ChecklistService.flightChecklist(targetFlightNum).then(function(flightTasks) {
			expect(flightTasks).toBeDefined();
			expect(flightTasks).not.toEqual(null);
			expect(flightTasks.length).toEqual(1);
			
			expect(flightTasks[0].title).toEqual(tasks[0].title);
			//expect(allTasks[0].dueDate).toEqual(tasks[0].dueDate); // BUG HERE
			expect(flightTasks[0].flightId).toEqual(tasks[0].flightId);
			expect(flightTasks[0].done).toEqual(tasks[0].done);
			
			// BUG: all() does not parse datetime values back to a datetime object
			var returnDate = new Date(flightTasks[0].dueDate)
			expect(returnDate).toEqual(tasks[0].dueDate);
			
			// ID is a guid. We can only test that it is there
			// and contains 4 (version 4)
			expect(flightTasks[0].id).toBeDefined();
			expect(flightTasks[0].id).not.toEqual(null);
			expect(flightTasks[0].id.includes("4")).toEqual(true);
		}, function(error) {
			expect(true).toEqual(false);
		});
		
	}));
	
	it ('Can delete a task', inject(function(ChecklistService) {
		// Generate the task
		var targetFlightNum = 1;
		var otherFlightNum = 2;
		var tasks =
			[
				ChecklistService.newTask(targetFlightNum),
				ChecklistService.newTask(otherFlightNum)
			];
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(2);
		
		// Set the Ids so we can delete unique items
		tasks[0].id = targetFlightNum;
		tasks[1].id = otherFlightNum;
	
		// Save the tasks
		ChecklistService.save(tasks);
		
		// Delete the tasks
		ChecklistService.delete(targetFlightNum);
		
		// Get the remaining tasks
		var remainingTasks = ChecklistService.all();
		expect(remainingTasks).toBeDefined();
		expect(remainingTasks).not.toEqual(null);
		expect(remainingTasks.length).toEqual(1);

		// Verify the data matches task 2		
		expect(remainingTasks[0].title).toEqual(tasks[1].title);
		//expect(remainingTasks[0].dueDate).toEqual(tasks[1].dueDate); // BUG HERE
		expect(remainingTasks[0].flightId).toEqual(tasks[1].flightId);
		expect(remainingTasks[0].id).toEqual(tasks[1].id);
		expect(remainingTasks[0].done).toEqual(tasks[1].done);
		
		// BUG: all() does not parse datetime values back to a datetime object
		var returnDate = new Date(remainingTasks[0].dueDate)
		expect(returnDate).toEqual(tasks[1].dueDate);
  	}));
  
  	it ('Can update a task', inject(function(ChecklistService) {
		// Generate the task
		var flightNum = 1;
		var tasks = [ ChecklistService.newTask(flightNum) ];
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);
		
		// Set some data so we can confirm the update
		var taskId = "123456";
		tasks[0].title = "My Title";
		tasks[0].id = taskId;
		
		// Save the tasks
		ChecklistService.save(tasks);
		
		// Update the tasks
		tasks[0].title = "A different title";
		ChecklistService.updateTask(taskId, tasks[0]);
		
		// Get the updated tasks
		var updatedTasks = ChecklistService.all();
		expect(updatedTasks).toBeDefined();
		expect(updatedTasks).not.toEqual(null);
		expect(updatedTasks.length).toEqual(1);

		// Verify the data matches task 2		
		expect(updatedTasks[0].title).toEqual(tasks[0].title);
		//expect(remainingTasks[0].dueDate).toEqual(tasks[0].dueDate); // BUG HERE
		expect(updatedTasks[0].flightId).toEqual(tasks[0].flightId);
		expect(updatedTasks[0].id).toEqual(tasks[0].id);
		expect(updatedTasks[0].done).toEqual(tasks[0].done);
		
		// BUG: all() does not parse datetime values back to a datetime object
		var returnDate = new Date(updatedTasks[0].dueDate)
		expect(returnDate).toEqual(tasks[0].dueDate);
  	}));
	  
	it ('Can append a task', inject(function(ChecklistService) {
		// Generate the first task
		var flight1 = 1;
		var tasks =  [ ChecklistService.newTask(flight1), ];
		
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);
			
		// Save the tasks
		ChecklistService.save(tasks);
		
		// Create the second task	
		var flight2 = 2;
		var newTask = ChecklistService.newTask(flight2)
		tasks.push(newTask);
			
		// Append the tasks
		ChecklistService.append(newTask);
		
		// Get all of the stored tasks
		var allTasks = ChecklistService.all();
		expect(allTasks).toBeDefined();
		expect(allTasks).not.toEqual(null);
		expect(allTasks.length).toEqual(2);

		// Verify the data matches
		for(var i = 0; i < 2; i++) {
			expect(allTasks[i].title).toEqual(tasks[i].title);
			//expect(remainingTasks[i].dueDate).toEqual(tasks[i].dueDate); // BUG HERE
			expect(allTasks[i].flightId).toEqual(tasks[i].flightId);
			expect(allTasks[i].id).toEqual(tasks[i].id);
			expect(allTasks[i].done).toEqual(tasks[i].done);
			
			// BUG: all() does not parse datetime values back to a datetime object
			var returnDate = new Date(allTasks[i].dueDate)
			expect(returnDate).toEqual(tasks[i].dueDate);
		}	
  	}));
	  
	it ('Can parse a task', inject(function(ChecklistService) {
		// Generate the first task
		var flight1 = 1;
		var tasks =  [ ChecklistService.newTask(flight1), ];
		
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);
		var origTask = tasks[0];
			
		// Save the task
		ChecklistService.save(tasks);
		
		// Get the stored task
		var allTasks = ChecklistService.all();
		expect(allTasks).toBeDefined();
		expect(allTasks).not.toEqual(null);
		expect(allTasks.length).toEqual(1);
		var parsedTask = ChecklistService.parse(allTasks[0]);

		// Verify the data matches
		expect(parsedTask.title).toEqual(origTask.title);
		expect(parsedTask.dueDate).toEqual(origTask.dueDate);
		expect(parsedTask.flightId).toEqual(origTask.flightId);
		expect(parsedTask.id).toEqual(origTask.id);
		expect(parsedTask.done).toEqual(origTask.done);
  	}));
	  
	it ('Cannot retrieve tasks for missing flight', inject(function(ChecklistService) {
		// Generate the first task
		var flight1 = 1;
		var missingFlight = 2;
		var tasks =  [ ChecklistService.newTask(flight1), ];
		
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);
			
		// Save the task
		ChecklistService.save(tasks);
		
		// Get the stored tasks
		ChecklistService.flightChecklist(missingFlight).then(function(foundTasks) {
			expect(allTasks).toBeUndefined();
			expect(allTasks).toEqual(null);
			expect(allTasks.length).toEqual(0);
		}, function(error) {
			expect(error).toBeDefined();
		});
  	}));
	
	it ('Cannot delete a missing task', inject(function(ChecklistService) {
		// Generate the first task
		var flight1 = 1;
		var missingTaskId = 2;
		var tasks =  [ ChecklistService.newTask(flight1), ];
		
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);
		var origTask = tasks[0];
			
		// Save the task
		ChecklistService.save(tasks);
		
		// Attempt to delete the missing task
		ChecklistService.delete(missingTaskId);
		
		// Get the stored task
		var allTasks = ChecklistService.all();
		expect(allTasks).toBeDefined();
		expect(allTasks).not.toEqual(null);
		expect(allTasks.length).toEqual(1);
		var remainingTask = allTasks[0];

		// Verify the data matches
		expect(remainingTask.title).toEqual(origTask.title);
		//expect(parsedTask.dueDate).toEqual(origTask.dueDate); // BUG HERE
		expect(remainingTask.flightId).toEqual(origTask.flightId);
		expect(remainingTask.id).toEqual(origTask.id);
		expect(remainingTask.done).toEqual(origTask.done);
		
		// BUG: all() does not parse datetime values back to a datetime object
		var returnDate = new Date(remainingTask.dueDate)
		expect(returnDate).toEqual(origTask.dueDate);
  	}));
	  
	it ('Cannot update a missing task', inject(function(ChecklistService) {
		// Generate the first task
		var flight1 = 1;
		var missingTaskId = 2;
		var tasks =  [ ChecklistService.newTask(flight1), ];
		
		expect(tasks).toBeDefined();
		expect(tasks).not.toEqual(null);
		expect(tasks.length).toEqual(1);
		tasks[0].id = 1;
		var origTask = tasks[0];
			
		// Save the task
		ChecklistService.save(tasks);
		
		// Create a new task and attempt to update the original
		var newTask = ChecklistService.newTask(flight1);
		newTask.id = 2;
		ChecklistService.updateTask(newTask.id, newTask);
		
		// Get all of the stored tasks
		var allTasks = ChecklistService.all();
		expect(allTasks).toBeDefined();
		expect(allTasks).not.toEqual(null);
		expect(allTasks.length).toEqual(1);
		var storedTask = allTasks[0];

		// Verify the data matches
		expect(storedTask.title).toEqual(origTask.title);
		//expect(parsedTask.dueDate).toEqual(origTask.dueDate); // BUG HERE
		expect(storedTask.flightId).toEqual(origTask.flightId);
		expect(storedTask.id).toEqual(origTask.id);
		expect(storedTask.done).toEqual(origTask.done);
		
		// BUG: all() does not parse datetime values back to a datetime object
		var returnDate = new Date(storedTask.dueDate)
		expect(returnDate).toEqual(origTask.dueDate);
  	}));
	  
	it ('Cannot load tasks if none in storage', inject(function(ChecklistService) {
		// Get all of the stored tasks
		var allTasks = ChecklistService.all();
		expect(allTasks).toEqual(null);
  	}));
	  
	it ('Cannot parse an undefined task', inject(function(ChecklistService) {
		var parsedTask = ChecklistService.parse(null);
		expect(parsedTask).toEqual(null);
  	}));

});
