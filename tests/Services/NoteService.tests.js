// According to this stack overflow post http://stackoverflow.com/a/31361163
// PhantomJS doesn't implement the String.includes method
// The following is the code that implements the includes method
if (!String.prototype.includes) {
  String.prototype.includes = function() {'use strict';
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}

describe('Note Service Tests', function() {
	var NoteService;
	
	beforeEach(module('FlightPub.services'));
	beforeEach(inject(function($injector) {
		NoteService = $injector.get('NoteService');
		
		// Clear local storage so it doesn't affect other tests
    	localStorage.clear();
	}));

	it ('Can create a new note', inject(function(NoteService) {
		var flightId = 1;
		
		// Define the expected values
		var expectedNote = new Note();
		expectedNote.body = "New Note"
		expectedNote.flightId = flightId;

		// Create the actual note		
		var actualNote = NoteService.newNote(flightId);
		expect(actualNote).toBeDefined();
		expect(actualNote).not.toEqual(null);

		// Check the note data
		expect(actualNote.body).toEqual(expectedNote.body);
		expect(actualNote.flightId).toEqual(expectedNote.flightId);
		
		// We can only check for the existence of the GUID
		expect(actualNote.guid).toBeDefined();
		expect(actualNote.guid).not.toEqual(null);
		expect(actualNote.guid.length).toEqual(36);
		expect(actualNote.guid.includes('4')).toEqual(true);
	}));
	
	it ('Can save and load notes from local storage', inject(function(NoteService) {
		// Create the original note
		var origNote = NoteService.newNote(1);
		expect(origNote).toBeDefined();
		expect(origNote).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ origNote ];
		NoteService.save(notes);
		
		// Load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toBeDefined();
		expect(storedNotes).not.toEqual(null);
		expect(storedNotes.length).toEqual(1);
		var actualNote = storedNotes[0];

		// Check the note data
		expect(actualNote.body).toEqual(origNote.body);
		expect(actualNote.flightId).toEqual(origNote.flightId);
		expect(actualNote.guid).toEqual(origNote.guid);
	}));
	
	it ('Can load notes for a given flight id', inject(function(NoteService) {
		// Create the original note
		var flightId = 1;
		var origNote = NoteService.newNote(flightId);
		expect(origNote).toBeDefined();
		expect(origNote).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ origNote ];
		NoteService.save(notes);
		
		// Load the note
		NoteService.flightNotes(flightId).then(function(storedNotes) {
			expect(storedNotes).toBeDefined();
			expect(storedNotes).not.toEqual(null);
			expect(storedNotes.length).toEqual(1);
			var actualNote = storedNotes[0];
	
			// Check the note data
			expect(actualNote.body).toEqual(origNote.body);
			expect(actualNote.flightId).toEqual(origNote.flightId);
			expect(actualNote.guid).toEqual(origNote.guid);
		}, function(error) {
			expect(error).toBeUndefined();
		});
	}));
	
	it ('Can append a note', inject(function(NoteService) {
		// Create the original note
		var note1 = NoteService.newNote(1);
		var note2 = NoteService.newNote(1);
		expect(note1).toBeDefined();
		expect(note1).not.toEqual(null);
		expect(note2).toBeDefined();
		expect(note2).not.toEqual(null);
		
		// Save the first note to local storage
		var notes = [ note1 ];
		NoteService.save(notes);
		
		// Append the second note
		notes.push(note2);
		NoteService.append(note2);
		
		// Load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toBeDefined();
		expect(storedNotes).not.toEqual(null);
		expect(storedNotes.length).toEqual(2);
		
		for (var i = 0; i < 2; i++) {
			var expectedNote = notes[i];
			var actualNote = storedNotes[i];
	
			// Check the note data
			expect(actualNote.body).toEqual(expectedNote.body);
			expect(actualNote.flightId).toEqual(expectedNote.flightId);
			expect(actualNote.guid).toEqual(expectedNote.guid);
		}
	}));
	
	it ('Can update a note', inject(function(NoteService) {
		// Create the original note
		var origNote = NoteService.newNote(1);
		expect(origNote).toBeDefined();
		expect(origNote).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ origNote ];
		NoteService.save(notes);
		
		// Update the note
		origNote.body = "Updated Note";
		NoteService.updateNote(origNote.guid, origNote);
		
		// Load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toBeDefined();
		expect(storedNotes).not.toEqual(null);
		expect(storedNotes.length).toEqual(1);
		var actualNote = storedNotes[0];

		// Check the note data
		expect(actualNote.body).toEqual(origNote.body);
		expect(actualNote.flightId).toEqual(origNote.flightId);
		expect(actualNote.guid).toEqual(origNote.guid);
	}));
	
	it ('Can delete a note', inject(function(NoteService) {
		// Create the original note
		var note1 = NoteService.newNote(1);
		var note2 = NoteService.newNote(1);
		expect(note1).toBeDefined();
		expect(note1).not.toEqual(null);
		expect(note2).toBeDefined();
		expect(note2).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ note1, note2 ];
		NoteService.save(notes);
		
		// Delete the first note
		NoteService.delete(note1.guid);
		
		// Load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toBeDefined();
		expect(storedNotes).not.toEqual(null);
		expect(storedNotes.length).toEqual(1);
		var actualNote = storedNotes[0];

		// Check the note data
		expect(actualNote.body).toEqual(note2.body);
		expect(actualNote.flightId).toEqual(note2.flightId);
		expect(actualNote.guid).toEqual(note2.guid);
	}));
	
	it ('Cannot load notes for a missing flight', inject(function(NoteService) {
		// Create the original note
		var flightId = 1;
		var missingFlightId = 2;
		var origNote = NoteService.newNote(flightId);
		expect(origNote).toBeDefined();
		expect(origNote).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ origNote ];
		NoteService.save(notes);
		
		// Load the note
		NoteService.flightNotes(missingFlightId).then(function(storedNotes) {
			expect(storedNotes).toBeUndefined();
			expect(storedNotes).toEqual(null);
			expect(storedNotes.length).toEqual(0);
		}, function(error) {
			expect(error).toBeDefined();
		});
	}));
	
	it ('Cannot update a missing note', inject(function(NoteService) {
		// Create the original note
		var origNote = NoteService.newNote(1);
		expect(origNote).toBeDefined();
		expect(origNote).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ origNote ];
		NoteService.save(notes);
		
		// Update the note
		origNote.body = "Updated Note";
		NoteService.updateNote(0, origNote);
		
		// Reset note
		origNote.body = "New Note";
		
		// Load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toBeDefined();
		expect(storedNotes).not.toEqual(null);
		expect(storedNotes.length).toEqual(1);
		var actualNote = storedNotes[0];

		// Check the note data
		expect(actualNote.body).toEqual(origNote.body);
		expect(actualNote.flightId).toEqual(origNote.flightId);
		expect(actualNote.guid).toEqual(origNote.guid);
	}));
	
	it ('Cannot delete a missing note', inject(function(NoteService) {
		// Create the original note
		var note1 = NoteService.newNote(1);
		expect(note1).toBeDefined();
		expect(note1).not.toEqual(null);
		
		// Save the note to local storage
		var notes = [ note1 ];
		NoteService.save(notes);
		
		// Delete the first note
		NoteService.delete(0);
		
		// Load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toBeDefined();
		expect(storedNotes).not.toEqual(null);
		expect(storedNotes.length).toEqual(1);
		var actualNote = storedNotes[0];

		// Check the note data
		expect(actualNote.body).toEqual(note1.body);
		expect(actualNote.flightId).toEqual(note1.flightId);
		expect(actualNote.guid).toEqual(note1.guid);
	}));
	
	it ('Cannot load notes if none are in storage', inject(function(NoteService) {
		// Attempt to load the note
		var storedNotes = NoteService.all();
		expect(storedNotes).toEqual(null);
	}));

});
