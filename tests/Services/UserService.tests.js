describe('User Service Tests', function() {
	var UserService;
	var StorageService;
	var _TokenService;
	var _ApiService;
	
	var _user;
	
	beforeEach(module('FlightPub-Constants'));
	beforeEach(module('FlightPub.services'));
	beforeEach(inject(function($injector) {
		UserService = $injector.get('UserService');
		StorageService = $injector.get('StorageService');
		_TokenService = $injector.get('TokenService');
		_ApiService = $injector.get('ApiService');
		
		_user = new User();
		_user.userName = "swaggins@hotmail.com";
		_user.firstName = "Frodo";
		_user.lastName = "Baggins";
		_user.dateOfBirth = new Date(2015, 10,4);
		_user.postalAddress =
			{
				streetNumber: 123,
				streetName: "Bag End",
				city: "The Shire",
				state: "NSW",
				country: "Australia"
			};
		_user.country = "Australia";
		
		// Clear local storage so it doesn't affect other tests
    	localStorage.clear();
	}));
	
	it ('Can save and load a user from local storage', inject(function(UserService) {
		// Save the user
		UserService.save(_user);
		
		// Load the user data
		var storedUser = UserService.all();
		expect(storedUser).toBeDefined();
		expect(storedUser).not.toEqual(null);
		
		// Verify the user data
		expect(storedUser.userName).toEqual(_user.userName);
		expect(storedUser.firstName).toEqual(_user.firstName);
		expect(storedUser.lastName).toEqual(_user.lastName);
		expect(storedUser.dateOfBirth).toEqual(_user.dateOfBirth);
		expect(storedUser.postalAddress).toEqual(_user.postalAddress);
		expect(storedUser.country).toEqual(_user.country);
	}));
	
	it ('Can parse a user', inject(function(UserService, StorageService) {
		// Save the user
		UserService.save(_user);
		
		// Load the user data
		var objectData = StorageService.deserialise('user');
		expect(objectData).toBeDefined();
		expect(objectData).not.toEqual(null);
		var parsedUser = UserService.parse(objectData);
		
		// Verify the user data
		expect(parsedUser.userName).toEqual(_user.userName);
		expect(parsedUser.firstName).toEqual(_user.firstName);
		expect(parsedUser.lastName).toEqual(_user.lastName);
		expect(parsedUser.dateOfBirth).toEqual(_user.dateOfBirth);
		expect(parsedUser.postalAddress).toEqual(_user.postalAddress);
		expect(parsedUser.country).toEqual(_user.country);
	}));
	
	it ('Cannot load a missing user', inject(function(UserService) {
		// Attempt to load the user data
		var storedUser = UserService.all();
		expect(storedUser).toBe(null);
	}));
	
	
	it ('Can download a user’s data from the ZotCode server', inject(function(FlightService) {
		
		// Mock the data result
		var mockUserData = new User();
		mockUserData.userName = "swaggins@hotmail.com";
		mockUserData.firstName = "Frodo";
		mockUserData.lastName = "Baggins";
		mockUserData.dateOfBirth = new Date(2015, 10,4);
		mockUserData.postalAddress =
			{
				streetNumber: 123,
				streetName: "Bag End",
				city: "The Shire",
				state: "NSW",
				country: "Australia"
			};
		mockUserData.country = "Australia";
		
		var mockUserResult = {
			data: mockUserData
		};
		
		// Store the user token
		var token = "ABC123";
		_TokenService.save(token);
		
		// Mock the Api Service call because $q doesn't work
		spyOn(_ApiService, 'apiGet').and.callFake(function() {
			return {
                then: function (callback) {
                    return callback(mockUserResult);
                }
            };
		});
		
		// Call the function
		UserService.download();
		
		// Retrieve the user data from local storage
		var downloadedUser = UserService.all();
		
		expect(downloadedUser).toBeDefined();
		expect(downloadedUser).not.toEqual(null);
		
		expect(downloadedUser).toEqual(mockUserData);
	}));
	
	it ('Cannot parse an undefined flight', inject(function(UserService) {
		var parsedTask = UserService.parse(null);
		expect(parsedTask).toBeUndefined();
  	}));
	
});
