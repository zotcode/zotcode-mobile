describe('Availability Tests', function(){
   
    it ('Can make an Availability Object', function() {
        var expected = new Availability();
		expected.id = 0;
		expected.flightId = 0;
		expected.seatClass = "Business";
		
		var actual = new Availability();

        expect(actual).toEqual(expected);
    });
	
});