describe('Ticket Tests', function(){
   
    it ('Can make an Ticket Object', function() {
        var expected = new Ticket();
		expected.id = 0;
		expected.ticketCode = "";
		expected.ticketName = "";
		expected.ticketPrice = 0;
		expected.allocatedSeatId = 0;
		
		var actual = new Ticket();

        expect(actual).toEqual(expected);
    });
	
});