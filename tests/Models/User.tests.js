describe('User Tests', function() {
   
    it ('User full name', function() {
        var user = new User();
		user.firstName = "John";
		user.lastName = "Doe";
        expect(user.fullName()).toEqual("John Doe");
    });
	
	it ('User address', function() {
        var user = new User();
		user.postalAddress =
		{
			streetNumber: "123",
			streetName: "Fake Street",
			city: "Newcastle",
			state: "NSW",
			country: "Australia",
		};
		
		var addressString = "123 Fake Street\nNewcastle, NSW\nAustralia ";
		
        expect(user.address()).toEqual(addressString);
    });
	
});