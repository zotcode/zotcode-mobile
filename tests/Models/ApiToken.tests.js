describe('API Token Tests', function(){
   
    it ('Can make an API Token Object', function() {
        var expected = new ApiToken();
		expected.access_token = "";
		expected.token_type = "bearer";
		expected.expires_in = 5183999; // 60 Days
		expected.userName = "user";
		
		var actual = new ApiToken();

		// Do this after so the dates are as close as possible
		expected.issued = new Date();
		expected.expires = new Date();

        expect(actual).toEqual(expected);
    });
	
});