describe('Checklist Task Tests', function(){
   
    // Colour
    
    it ('Checklist task done colour is green', function() {
        var task = new ChecklistTask();
        task.done = true;
        expect(task.color()).toEqual("#00FF00");
    });
   
    it ('Checklist task overdue colour is red', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
        expect(task.color()).toEqual("#FF0000");
    });
    
    it ('Checklist task warning colour is yellow', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
        expect(task.color()).toEqual("#FFFF00");
    });
    
    it ('Checklist task normal colour is white', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 3);
        expect(task.color()).toEqual("#FFFFFF");
    });
    
    // Icons
    
    it ('Checklist task done icon is "done"', function() {
        var task = new ChecklistTask();
        task.done = true;
        expect(task.icon()).toEqual("done");
    });
    
    it ('Checklist task warning icon is "warn"', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
        expect(task.icon()).toEqual("warn");
    });
    
    it ('Checklist task todo icon is "todo"', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 3);
        expect(task.icon()).toEqual("todo");
    });
    
    // Overdue status
    
    it ('Checklist task is not overdue when done', function() {
        var task = new ChecklistTask();
        task.done = true;
        expect(task.overdue()).toEqual(false);
    });
    
    it ('Checklist task is not overdue when not done but not out of time', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
        expect(task.overdue()).toEqual(false);
    });
    
    it ('Checklist task is overdue when not done and out of time', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
        expect(task.overdue()).toEqual(true);
    });
    
    // Warning status
    
    it ('Checklist task does not display warning when done', function() {
        var task = new ChecklistTask();
        task.done = true;
        expect(task.warning()).toEqual(false);
    });
    
    it ('Checklist task does not display warning when not done but not within warning window', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 3);
        expect(task.warning()).toEqual(false);
    });
    
    it ('Checklist task displays warning when not done and within warning window', function() {
        var task = new ChecklistTask();
        var now = new Date();
        task.done = false;
        task.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
        expect(task.warning()).toEqual(true);
    });

});