// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var moduleName = 'FlightPub';
var requires = ['ionic', 'uiGmapgoogle-maps', 'FlightPub-Constants', 'FlightPub.services', 'FlightPub.controllers'];
var app = angular.module(moduleName, requires);
var serviceModule = angular.module('FlightPub.services', []);
var controllerModule = angular.module('FlightPub.controllers', []);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
});