/**
 * Captures a subset of the data in the FlightPub database schema
 * for defining a user of the system, but from a Mobile perspective
 */
function User() {
	
	this.userName = "";
	this.firstName = "";
	this.lastName = "";
	this.dateOfBirth = new Date();
	
	// These could be split into separate classes, but since
	// we really only need one, it should be fine to just use them
	// as strings
	this.postalAddress = {};
	this.country = "";
}

User.prototype.fullName = function() {
	return this.firstName + " " + this.lastName;
}

User.prototype.address = function() {
	return this.postalAddress.streetNumber + " " + 
		this.postalAddress.streetName + "\n" +
		this.postalAddress.city + ", " +
		this.postalAddress.state + "\n" +
		this.postalAddress.country + " ";
}