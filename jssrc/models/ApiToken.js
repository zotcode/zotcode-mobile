/**
 *{
  "access_token": "V1oosFcK9W4pNukMx-cuVfSUd7BE0hjVrTO1SVK_JX0TLUAi-w-5BM_FF2rYr-pMxlmp98pMlX7RG8JeOmjt09Pc0cWkNzxHMRpoJgKgy6Jn5Dancqqtss-GJKpzmY63pNyKFaalACdz7Y2QJmNwySWVaIbQc_b9QtTRppv57Jkl0dDvHUTN8g7ilxA1hniUkx6pCdw3ltUe5wNxywpMGDR9ZPw80H_HSTuYl6TtWqc6UMkTCd3xm_OawLPRG7Q9Cu9VKAIndWEtOpcuQ0xgtSldA9AIFR9OIA1BykL2PoqQP47mTBIP18VqnNn1SDfZ8AJUvWRvgttXv0rJEGgobkiOzU8tLzCrItopqCS4IRoywbic30tA-XCOmvy6cKCIv-mx0Wl_EebYYg8PlP-Lf5Gs1n3CaHJwLlrddpNYOPSwhKgr1pNGkcq0fZXhCD4326SvxwLM8pKR_0nKyYtcO6rGLIuRPTZDtlPIAwEV2QM",
  "token_type": "bearer",
  "expires_in": 5183999,
  "userName": "shartcher",
  ".issued": "Mon, 31 Aug 2015 11:00:12 GMT",
  ".expires": "Fri, 30 Oct 2015 11:00:12 GMT"
}
 */
 
 
function ApiToken() {
   this.access_token = "";
   this.token_type = "bearer";
   this.expires_in = 5183999; // 60 Days
   this.userName = "user";
   this.issued = new Date();
   this.expires = new Date();
 }