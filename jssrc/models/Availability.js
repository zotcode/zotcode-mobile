/**
 * Captures a subset of the data in the FlightPub database schema
 * pertaining to Seat Availability
 */
function Availability() {
	this.id = 0;
	this.flightId = 0;
	this.seatClass = "Business";
}