/**
 * Defines a memo note, stored in local storage against a flight
 */
function Note() {
	this.guid = "";
	this.body = "";
	this.flightId = 0;
}