/**
 * Captures the same data that exists in the FlightPub database schema
 * pertaining to Flight information
 */
function Flight() {
	
	// FlightPub Flight Schema
	this.id = 0;
	this.code = 'BF123';

	this.departureDate = new Date();
	this.departureLocation = 'Sydney';

	this.arrivalDate = new Date();
	this.arrivalLocation = 'Tokyo';
	
	// Optionals
	this.arriveStopOverDate = new Date();
	this.stopOverLocation = 'Brisbane';
	this.departStopOverDate = new Date();
}

/**
 * Calculates the duration of the first leg of the flight
 */
Flight.prototype.leg1Duration = function() {
	if (this.stopOverDate) {
		return this.stopOverDate - this.departureDate;
	} else {
		return this.arrivalDate - this.departureDate;
	}
}

/**
 * Calculates the duration of the second leg of the flight,
 * or 0 if it is a direct flight
 */
Flight.prototype.leg2Duration = function() {
	if (this.stopOverDate) {
		return this.arrivalDate - this.stopOverDate;
	} else {
		return 0; // Since there is no second leg
	}
}

/**
 * Determines whether the flight has a stopover location or not
 */
Flight.prototype.hasStopover = function() {
	return (this.stopOverLocation && this.stopOverLocation.length != 0);
}

Flight.prototype.daysUntilFlight = function() {
	var now = new Date();
	var milliPerDay = 1000*60*60*24; // Milliseconds in a day
	
	return Math.floor((this.departureDate < now) ? 0.0 :
		(this.departureDate.getTime() - now.getTime()) / milliPerDay);
}