/**
 * Defines the model for storing checklist task items
 * against a flight
 */
function ChecklistTask() {
	this.title = "";
	this.dueDate = new Date();
	this.done = false;
	this.id = 0;
	this.flightId = 0;
}

/**
 * Determines the color that should be displayed by a checklist
 * item based on whether it is "done" or how long until it
 * becomes overdue
 */
ChecklistTask.prototype.color = function() {
	var now = new Date();
	
	if (this.done) { return "#00FF00"; }
	else if (this.overdue()) { return "#FF0000"; }
	else if (this.warning()) { return "#FFFF00"; }
	else { return "#FFFFFF"; }
}

/**
 * Determines the icon that should be displayed by a checklist
 * item based on whether it is "done" or how long until it
 * becomes overdue
 */
ChecklistTask.prototype.icon = function() {
	var now = new Date();

	if (this.done) { return "done" }
	else if (this.warning()) { return "warn" }
	else {return "todo"; }
}

/**
 * Determines whether the checklist task is now overdue
 */
ChecklistTask.prototype.overdue = function() {
	var now = new Date();
	return (!this.done && this.dueDate < now) || false;
}

/**
 * Determines whether to display a warning message indicating that the
 * checklist item will be overdue in 24 hours
 */
ChecklistTask.prototype.warning = function() {
	var now = new Date();
	var oneDay = 1000*60*60*24; // One day in miliseconds since date subtraction results in miliseconds
	return !this.done && ((this.dueDate - now) < (oneDay)) || false;
}