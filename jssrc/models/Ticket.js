/**
 * Contains a subset of the data in the FlightPub database schema
 * pertaining to Ticket information
 */
function Ticket() {
	this.id = 0;
	this.ticketCode = "";
	this.ticketName = "";
	this.ticketPrice = 0;
	this.allocatedSeatId = 0; // Availability.js
}