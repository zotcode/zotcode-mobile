serviceModule.service('ApiService', function($http, $q, zotCodeApiUrl) {
	return {
		
		get: function(apiUrl, params) {
			var deferred = $q.defer();
			var url = zotCodeApiUrl + apiUrl;
			
			if (params) url += this.parameterise(params);
			
			console.log("Sending GET to: " + url);
			
			$http.get(url)
				.success(function(data) {
					deferred.resolve(data);
				})
				.error(function(err) {
					deferred.reject(err);
				});
			
			return deferred.promise;
		},
		
		post: function(apiUrl, params) {
			var deferred = $q.defer();
			var url = zotCodeApiUrl + apiUrl;
			
			var str = this.parameterise(params)
			console.log("Sending POST to: " + url + " with " + str);
			
			var req = {
				method: 'POST',
				url: url,
				data: str,
				headers: { 'Content-Type': 'x-www-form-urlencoded', 'Access-Control-Allow-Origin': "*" }
			};
			
			$http(req).then(function(data) {
				deferred.resolve(data);
			}, function(err) {
				deferred.reject(err);
			})
			
			return deferred.promise;
		},
		
		parameterise: function(paramList) {
			var p = [];
			for (var key in paramList) {
				p.push(encodeURIComponent(key) + '=' + encodeURIComponent(paramList[key]));
			}
			return p.join('&');
		},
		
		apiGet: function(apiUrl, accessToken) {
			var deferred = $q.defer();
			var url = zotCodeApiUrl + apiUrl;
			
			console.log("Sending GET to: " + url);
			
			var req = {
				headers: {'Authorization': 'Bearer ' + accessToken},
				method: 'GET',
				url: url
			};
			
			$http(req)
				.then(function(data) {
					deferred.resolve(data);
				}, function(err) {
					deferred.reject(err);
				});
			
			return deferred.promise;
		}
	}
});