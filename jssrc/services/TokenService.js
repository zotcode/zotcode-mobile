serviceModule.service('TokenService', function(StorageService) {
	return {
		
		/**
		 * Gets all users stored in local storage. There should
		 * only be one user in storage at any given time
		 */
		all: function() {
			var token = StorageService.deserialise('token');
			return token;
		},
		
		save: function(token) {
			StorageService.serialise('token', token);
		}
		
	}
});