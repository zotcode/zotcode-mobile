/**
 * The Checklist Service is responsible for the management of Checklist Task Items
 * stored in local storage.
 * 
 * Injections:
 * 	- GuidService - Used for generating unique IDs for new tasks
 */
serviceModule.service('ChecklistService', function($q, GuidService, StorageService) {
	return {
		
		/**
		 * Creates a new task with due date 2 days from now
		 */
		newTask: function(flightId) {
			var item = new ChecklistTask();
			var now = new Date();
			
			item.title = "New Task";
			item.dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 2);
			item.id = GuidService.create();
			item.flightId = flightId;
			
			return item;
		},
		
		/**
		 * Generates a list of tasks that are generally created when booking
		 * a Flight
		 */
		generateTasks: function() {
			var checklist = [];
			
			var passport = new ChecklistTask();
			passport.title = "Passport";
			
			var visa = new ChecklistTask();
			visa.title = "Visa";
			
			// Push to checklist
			checklist.push(passport);
			checklist.push(visa);
			
			return checklist;
		},
		
		all: function() {
			var checklists = StorageService.deserialise('checklist');
			return checklists;
		},
		
		flightChecklist: function(flightId) {
			var dfd = $q.defer()

			var flightChecklist = [];
			var checklists = this.all();
			var self = this;

			if (checklists instanceof Array) {
				checklists.forEach(function(task) {
					
					if (task.flightId == flightId) {
						var t = self.parse(task);
						if (t) flightChecklist.push(t);
					}
				})
			}
			
			dfd.resolve(flightChecklist);
			return dfd.promise;
		},
		
		save: function(checklists) {
			StorageService.serialise('checklist', checklists);
		},
		
		delete: function(checklistId) {
			var checklists = this.all();
			
			if (checklists instanceof Array) {
				checklists.forEach(function(task, i) {
					if (task.id == checklistId) checklists.splice(i, 1)
				})
			}
			
			this.save(checklists);
		},
		
		updateTask: function(taskId, newTask) {

			// Search all flights for id and replace			
			var checklists = this.all();
			if (checklists instanceof Array) {
				checklists.forEach(function(task, i) {
					if (task.id == taskId) checklists[i] = newTask;
				})
			}
			
			this.save(checklists);
		},
		
		append: function(task) {
			var checklists = this.all();
			if (checklists) checklists.push(task)
			else {
				checklists = [];
				checklists.push(task);
			}
			this.save(checklists);
		},
		
		parse: function(checkObj) {
			
			if (!checkObj) return null;
			
			var checklist = new ChecklistTask();
			checklist.title = checkObj.title;
			checklist.dueDate = new Date(checkObj.dueDate);
			checklist.done = checkObj.done;
			checklist.id = checkObj.id;
			checklist.flightId = checkObj.flightId;
			
			return checklist;
		}
		
	}
});