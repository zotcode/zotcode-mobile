/**
 * The Flight Service is responsible for retriving and sending Flight data to local storage
 * 
 * Injections:
 * 	- $q - Used for making "promises"
 * 	- ChecklistService - Used to generate a list of checklist tasks associated with flights
 * 	- StorageService - Responsible for the actual retrieval and storage of data
 */
serviceModule.service('FlightService', function($q, ChecklistService, StorageService, ApiService, TokenService, NoteService) {
	return {
		
		/**
		 * Generates a mock flight for testing
		 */
		generate: function() {
			// Test Data for API
			var flight = new Flight();
			flight.id = 0;
			flight.code = "BF256";
			flight.departureDate = new Date(2015, 1, 1);
			flight.departureLocation = "Australia";
			flight.arrivalDate = new Date(2015, 1, 2);
			flight.arrivalLocation = "Spain";
			flight.checklist = ChecklistService.generateTasks();
			
			return flight;
			
			// flights = [
			// 	{id: 1, name: 'BF256', depLocation: 'Sydney', depDate: new Date(2015, 1, 1), arrLocation: 'Tokyo', arrDate: new Date(2015, 1, 2),  
			// 		checklist: [
			// 			{id: 1, name: "Passport", done: true,  due: new Date(2015, 1, 2) },
			// 			{id: 2, name: "Visa",     done: false, due: new Date(2015, 1, 1) },
			// 			{id: 3, name: "Accommodation", done: false, due: new Date() + 2}
			// 		]
			// 	},
				
			// 	{id: 2, name: 'WC962', depLocation: 'Melbourne', depDate: new Date(2015, 2, 6),  arrLocation: 'England',   arrDate: new Date(2015, 2, 7)  },
			// 	{id: 3, name: 'RC240', depLocation: 'Tokyo',     depDate: new Date(2015, 3, 20), arrLocation: 'Hong Kong', arrDate: new Date(2015, 3, 21) },
			// 	{id: 4, name: 'AS124', depLocation: 'LAX',       depDate: new Date(2015, 4, 08), arrLocation: 'New York',  arrDate: new Date(2015, 4, 09),
			// 		checklist: [
			// 			{id: 1, name: "Passport", done: true,  due: new Date(2015, 1, 2) },
			// 			{id: 2, name: "Visa",     done: false, due: new Date(2015, 1, 1) },
			// 			{id: 3, name: "Accommodation", done: false, due: new Date() + 2}
			// 		]	
			// 	}
			// ];	
			
		},
		
		/**
		 * Retrieves all flights stored for the user in local storage
		 */
		all: function() {
			var flights = StorageService.deserialise('flights');
 			return flights;
		},
		
		/**
		 * Saves a given list of flights to local storage. Note: This will overwrite
		 * any existing flight data
		 */
		save: function(flights) {
			StorageService.serialise('flights', flights);
		},
		
		/**
		 * Gets a particular flight with the given ID. Implemented using promises
		 */
		getFlight: function(flightId) {
			var dfd = $q.defer()
			var resolved = false;

			// Search all flights for id			
			var flights = this.all();
			
			if (flights instanceof Array) {
			
				flights.forEach(function(flight) {
					if (flight.id == flightId) { 
						dfd.resolve(flight)
						resolved = true;
					}
				})
			}
			
			// Reject the promise if not found
			if (!resolved) dfd.reject("Not Found");
			
			return dfd.promise
		},
		
		/**
		 * Update the flight with the given ID by overwriting it with
		 * new flight data
		 */
		updateFlight: function(flightId, newFlight) {

			// Search all flights for id and replace			
			var flights = this.all();
			if (flights instanceof Array) {
				flights.forEach(function(flight, i) {
					if (flight.id == flightId) flights[i] = newFlight;
				})
			}
			
			this.save(flights);
		},
		
		download: function() {
			var token = TokenService.all();
			var self = this;
			
			if (token) {
				console.log("Fetching flight data");
				ApiService.apiGet('api/flights', token.access_token).then(function(data) {
					
					var flights = data.data;
					
					if (flights instanceof Array) {
						flights.forEach(function(f) {
							// TODO: Don't erase the flight checklist/notes if they exist
							// Add empty checklist array
							f.checklist = [];
						})
					}
					
					self.save(flights);
					console.log("Download complete. " + flights.length + " flights downloaded");
				
				}, function(error) {
					console.log(error.data)
				});
			}
			
		},
		
		nextFlight: function() {
			var deferred = $q.defer();
			var flights = this.all();
			var minFlight = null;
			
			if (flights instanceof Array) {
				flights.forEach(function(f) {
					if (minFlight == null ||
						f.departureDate < minFlight.departureDate)
						minFlight = f;
				})
			}
			
			if (minFlight) deferred.resolve(this.parse(minFlight));
			else deferred.reject('No Flights for user');
			
			return deferred.promise;
		},
		
		parse: function(flightObj) {
			
			if (!flightObj) return;
			
			var flight = new Flight();
			flight.id = flightObj.id || -1;
			flight.code = flightObj.code;
			flight.departureDate = new Date(flightObj.departureDate);
			flight.departureLocation = flightObj.departureLocation;
			flight.arrivalDate = new Date(flightObj.arrivalDate);
			flight.arrivalLocation = flightObj.arrivalLocation;
			flight.arriveStopOverDate = flightObj.arriveStopOverDate;
			flight.stopOverLocation = flightObj.stopOverLocation;
			flight.departStopOverDate = flightObj.departStopOverDate;
			return flight;
		}
		
	}
});