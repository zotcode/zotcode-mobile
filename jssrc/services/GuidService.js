/**
 * Responsible for the creation of unique identifiers when storing in local storage
 */
serviceModule.service('GuidService', function() {
	return {
		
		/**
		 * Creates a new GUID as defined by RFC 4122
		 */
		create: function() {
			
			// GUID creating function developed in this Stack Overflow
			// post: http://stackoverflow.com/a/2117523
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
				return v.toString(16);
			})},
		
	};
})