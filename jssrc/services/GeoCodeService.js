serviceModule.service('GeoCodeService', function($q, $http, googleGeoCodeUrl, googleApiKey) {
	
	/**
	 * https://maps.googleapis.com/maps/api/geocode/
	 * json?
	 * components=locality:Tokyo
	 * &key=AIzaSyCpEO-lvvdn94Y_cg10IZzplZR-Ay7L1W4
	 * 
	 * https://maps.googleapis.com/maps/api/geocode/
	 * json?components=country:Spain
	 * &key=AIzaSyCpEO-lvvdn94Y_cg10IZzplZR-Ay7L1W4
	 */
	
	return {
		
		countryLatLong: function(country) {
			var deferred = $q.defer();
			var url = googleGeoCodeUrl + "json?components=country:" + country + "&key=" + googleApiKey;

			$http.get(url)
				.success( function(data) {
					deferred.resolve(data);
				})	
				.error(function(error) {
					deferred.reject(error);
				});

			return deferred.promise;
		},
		
		localityLatLong: function(locality) {
			var deferred = $q.defer();
			var url = googleGeoCodeUrl + "json?components=locality:" + locality + "&key=" + googleApiKey;
			$http.get(url)
				.success( function(data) {
					deferred.resolve(data);
				})	
				.error(function(error) {
					deferred.reject(error);
				});

			return deferred.promise;
		},
		
		lat: function(obj) {
			var lat = obj.results[0].geometry.location.lat;
			return lat;
		},
		
		long: function(obj) {
			var long = obj.results[0].geometry.location.lng;
			return long;
		},
		
		coords: function(obj) {
			
			var lat = this.lat(obj);
			var long = this.long(obj);
			
			return { lat: lat, long: long };
		}
		
	};
})