/* global ; */
/**
 * Responsible for the storage and retrieval of notes in local storage
 * 
 * Injection:
 * 	- $q - Used for implementing "promises"
 * 	- StorageDevice - The service responsible for actually reading/writing data
 * 	- GuidService - Responsible for the creation of unique IDs for notes
 */
serviceModule.service('NoteService', function($q, StorageService, GuidService) {
	return {
		
		/**
		 * Creates a new note for the given flight
		 */
		newNote: function(flightId) {
			var note = new Note();
			
			note.guid = GuidService.create();
			note.body = "New Note";
			note.flightId = flightId;
			
			return note;
		},
		
		/**
		 * Retrieves all notes stored in local storage, regardless of the
		 * flight stored against
		 */
		all: function() {
			var notes = StorageService.deserialise('notes');
			return notes;
		},
		
		/**
		 * Gets all notes associated with a particular flight id.
		 * Implemented using promises
		 */
		flightNotes: function(flightId) {
			var dfd = $q.defer()

			var flightNotes = [];
			var notes = this.all();

			if (notes instanceof Array) {
				notes.forEach(function(note) {
					if (note.flightId == flightId) flightNotes.push(note);
				})
			}
			
			dfd.resolve(flightNotes);
			return dfd.promise;
		},
		
		/**
		 * Saves a collection of notes to local storage. Note: this will
		 * overwrite any existing notes stored
		 */
		save: function(notes) {
			StorageService.serialise('notes', notes);
		},
		
		append: function(note) {
			var notes = this.all();
			if (notes) notes.push(note)
			else {
				notes = [];
				notes.push(note);
			}
			this.save(notes);
		},
		
		updateNote: function(noteId, newNote) {

			// Search all flights for id and replace			
			var notes = this.all();
			if (notes instanceof Array) {
				notes.forEach(function(note, i) {
					if (note.guid == noteId) notes[i] = newNote;
				})
			}
			
			this.save(notes);
		},
		
		/**
		 * Deletes a note from the local storage
		 */
		delete: function(noteId) {
			var notes = this.all();
			
			if (notes instanceof Array) {
				notes.forEach(function(note, i) {
					if (note.guid == noteId) notes.splice(i, 1)
				})
			}
			
			this.save(notes);
		}
		
	}
});