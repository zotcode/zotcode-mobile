/**
 * Provides a level of abstraction from the local storage medium.
 * Responsible for serialising and deserialising data stored in local
 * storage. Serialisation is implemented using JSON
 */
serviceModule.service('StorageService', function() {
	return {
		
		/**
		 * Loads the data stored under the given key and returns it
		 */
		load: function(key) {
			return window.localStorage[key];
		},
		
		/**
		 * Saves the data under the given key 
		 */
		save: function(key, data) {
			window.localStorage[key] = data;
		},
		
		/**
		 * Serialises the data in JSON and stored under the given key
		 */
		serialise: function(key, data) {
			this.save(key, angular.toJson(data));
		},
		
		/**
		 * Deserialises the JSON stored object at the given key and returns it
		 */
		deserialise: function(key) {
			var data = this.load(key);
			if (data) {
				return angular.fromJson(data);
			}
			
			return null;
		}
		
	}
});