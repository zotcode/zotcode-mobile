Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

serviceModule.service('PassportService', function(UserService, FlightService) {
	return {
		
		user: function() {
			return UserService.all();
		},
		
		flights: function() {
			return FlightService.all();
		},
		
		countries: function() {
			var countries = [];
			var flights = this.flights();
			
			if (flights instanceof Array) {
				flights.forEach(function(f) {
					if (!countries.contains(f.arrivalLocation))
						countries.push(f.arrivalLocation)
				});
			}
			
			return countries;
		}
		
	}
});

