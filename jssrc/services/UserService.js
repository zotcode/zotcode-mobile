serviceModule.service('UserService', function(StorageService, TokenService, ApiService) {
	return {
		
		/**
		 * Gets all users stored in local storage. There should
		 * only be one user in storage at any given time
		 */
		all: function() {
			var user = StorageService.deserialise('user');
			if (user) user = this.parse(user);
			return user;
		},
		
		save: function(user) {
			StorageService.serialise('user', user);
		},
		
		download: function() {
			var token = TokenService.all();
			var self = this;
			
			if (token) {
				console.log("Fetching user data");
				ApiService.apiGet('api/account/userinfo', token.access_token).then(function(data) {
					var user = data.data;
					if (user) {
						user = self.parse(user);
						self.save(user);
						console.log("Download complete.");
					}
				}, function(error) {
					console.log(error.data)
				});
			}
		},
		
		parse: function(userObj) {
			if (userObj) {
				var user = new User();
				user.userName = userObj.userName;
				user.firstName = userObj.firstName;
				user.lastName = userObj.lastName;
				user.dateOfBirth = new Date(userObj.dateOfBirth);
				user.postalAddress = userObj.postalAddress;
				user.country = userObj.postalAddress.country;
				return user;
			}
		}
		
		
	}
});