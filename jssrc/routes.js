var viewPath = './views';

app.config(function($stateProvider, $urlRouterProvider) {
  
  // Default Route
  $urlRouterProvider.otherwise('/home/dashboard')
  
  $stateProvider
  .state('home', {
    abstract: true,
    url: '/home',
    template: '<ion-nav-view></ion-nav-view>' // Required for abstract states
  })
  
  .state('home.login', {
    url: '/login',
    templateUrl: './views/home/login.html'
  })
  
  .state('home.dashboard', {
    url: '/dashboard',
    templateUrl: './views/home/dashboard.html'
  })
  
  .state('home.checklist', {
    url: '/checklist',
    templateUrl: './views/home/checklist.html'
  })
  
  // Flight Area
  .state('flight', {
    abstract: true,
    url: '/flight',
    template: '<ion-nav-view></ion-nav-view>' // Required for abstract states
  })
  
  .state('flight.index', {
    url: '',
    templateUrl: './views/flight/index.html'
  })
  
  .state('flight.details', {
    url: '/details/:flightId',
    templateUrl: './views/flight/details.html'
  })
  
  .state('flight.checklist', {
    url: '/checklist/:flightId',
    templateUrl: './views/flight/checklist.html'
  })
  
  .state('flight.notes', {
    url: '/notes/:flightId',
    templateUrl: './views/flight/notes.html'
  })
  
   .state('flight.poi', {
    url: '/poi/:flightId',
    templateUrl: './views/flight/poi.html'  
  })
  
  .state('flight.weather', {
    url: '/weather/:flightId',
    templateUrl: './views/flight/weather.html'  
  })
  
  
  // Passport Area
  .state('passport', {
    abstract: true,
    url: '/passport',
    template: '<ion-nav-view></ion-nav-view>' // Required for abstract states
  })
  
  .state('passport.index', {
    url: '/index',
    templateUrl: './views/passport/index.html'
  })
  
  .state('passport.details', {
    url: '/details',
    templateUrl: './views/passport/details.html'
  })
  
  .state('passport.countries', {
    url: '/countries',
    templateUrl: './views/passport/countries.html'
  })
  
  .state('passport.map', {
    url: '/map',
    templateUrl: './views/passport/map.html'
  })
  
  
  // Calendar Area
  .state('calendar', {
      abstract: true,
      url: '/calendar',
      template: '<ion-nav-view></ion-nav-view>'
  })
  
  .state('calendar.calendar', {
      url:'/calendar',
      templateUrl: './views/calendar/calendar.html'
  })
  
  // Settings Area
  .state('settings', {
      abstract: true,
      url: '/settings',
      template: '<ion-nav-view></ion-nav-view>'
  })
  
  .state('settings.index', {
      url:'/index',
      templateUrl: './views/settings/index.html'
  });
  
});
