controllerModule.controller('CalendarCtrl', function($scope, $state, FlightService){
        
    $scope.getFlights = function(){  
      $scope.flights = FlightService.all();      
    }
        
    $scope.insertToCalendar = function(title){
        var flight = getById($scope.flights, title);
        var options = window.plugins.calendar.getCalendarOptions();
        var success = function(message) { alert("Success: " + JSON.stringify(message) + "Flight added to Calendar"); };
        var error = function(message) { alert("Error: " + message); };
        window.plugins.calendar.createEventWithOptions(flight.code,
            flight.departureLocation,
            flight.cost,
            flight.departureDate,
            flight.arrivalDate,
            options,
            success,
            error);       
      window.plugins.calendar.openCalendar(flight.departureDate);   
    }
    //Search for specific event by title (code)
    function getById(input, id){
        var i=0, len=input.length;            
        for (; i<len; i++) {
            if (input[i].code == id) {
                
                return input[i];
            }
        }
        return null;
     }        
})



