// Taken From http://codepen.io/ionic/pen/uzngt
controllerModule.controller('MapCtrl', function($scope, GeoCodeService, PassportService) {
    
    /**
     * Test Data
     * 
     * https://maps.googleapis.com/maps/api/staticmap?
     * center=Brooklyn+Bridge,New+York,NY
     * &zoom=13
     * &size=600x300
     * &maptype=roadmap
     * &markers=color:blue%7Clabel:S%7C40.702147,-74.015794
     * &markers=color:green%7Clabel:G%7C40.711614,-74.012318
     * &markers=color:red%7Clabel:C%7C40.718217,-73.998284
     * 
     * https://maps.googleapis.com/maps/api/geocode/json?
     * components=locality:Tokyo
     * &key=AIzaSyCpEO-lvvdn94Y_cg10IZzplZR-Ay7L1W4
     */
    
    $scope.map = {
        center: { latitude: 45, longitude: -73 },
        zoom: 1,
        dragging: true
    };
    
    $scope.markers = [];
    
    PassportService.countries().forEach(function(loc) {
        
        GeoCodeService.localityLatLong(loc).then(function(data) {
            var jCoords = GeoCodeService.coords(data);
            var marker = {
                id: 0,
                coords: { latitude: jCoords.lat, longitude: jCoords.long },
            };
            
            $scope.markers.push(marker);
        });
    });
    
});