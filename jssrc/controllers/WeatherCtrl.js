 controllerModule.controller('WeatherCtrl', function($scope, $state, $http, FlightService, $ionicSlideBoxDelegate){
  var daysOfWeek = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
   
  //Workaround for Ionic-Slider-Box. Resets to prevent width being set to 0px
  $scope.$on('$ionicView.beforeEnter', function() {
    $ionicSlideBoxDelegate.update();
  });
 
  //Gets the current weather conditions and 5 day forcast at a particular location.
  $scope.getWeather = function(ln){
    var privateAPI = 'dab921898e95ab4c70bf19d4e8607';
    var baseURL = 'http://api.worldweatheronline.com/free/v2/weather.ashx';
    var days = "5";
    var format = 'json';
    var location = ln;
    
    //Retrieve data from API        
    $http.post(baseURL + "?q=" + location +"&num_of_days="+ days  +"&format=" +format+ "&key=" + privateAPI ).
      then(function(response) {
        var jason = JSON.stringify(response); 
        var data = JSON.parse(jason);
        
        //Get Current weather Conditions
        $scope.cCond = [];
        $scope.cCond.push({tempC:data.data.data.current_condition[0].temp_C, 
                          FeelsLikeC: data.data.data.current_condition[0].FeelsLikeC,
                          cloudcover: data.data.data.current_condition[0].cloudcover,
                          weatherIconUrl: data.data.data.current_condition[0].weatherIconUrl[0].value
                          });
                          
        //Get the forecast for next 5 days
        $scope.forecasts = [];
      
        //Add each day forcast to $scope
        for(var i = 0; i < parseInt(days); i++ ){
          var date = data.data.data.weather[i].date;
            $scope.forecasts.push({ day:  daysOfWeek[new Date(date).getDay()],  
                                    date: date,
                                    minC: data.data.data.weather[i].mintempC,
                                    maxC: data.data.data.weather[i].maxtempC,   
                                    uv: data.data.data.weather[i].uvIndex,   
                                    chanceOfRain: data.data.data.weather[i].hourly[4].chanceofrain,
                                    weatherIconUrl: data.data.data.weather[i].hourly[4].weatherIconUrl[0].value}); 
        }
      }, function(response) {
        //If error...
        console.log("ERROR: " + response);
        console.log("ERROR: " + JSON.stringify(response));
      });
    
  }
  
  //Get the arrival location of the selected flight
  $scope.getLocation = function(id){    
    FlightService.getFlight($state.params.flightId).then(function(flight) {
      $scope.getWeather(flight.arrivalLocation);
   });
  };	
})


  