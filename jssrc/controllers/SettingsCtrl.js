controllerModule.controller('SettingsCtrl', function($scope, $state, UserService, FlightService) {
  
  $scope.downloadUserData = function() {
	  UserService.download();
  };
  
  $scope.downloadFlightData = function() {
	  FlightService.download();
  };
  
  $scope.logOut = function() {
    localStorage.clear();
    $state.go('home.login');
  }
  
});