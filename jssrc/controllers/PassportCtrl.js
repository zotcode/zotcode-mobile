controllerModule.controller('PassportCtrl', function($scope, $state, $ionicHistory, PassportService) {
  // TODO: Implement controller functionality
  
  $scope.user = PassportService.user();
  $scope.flights = PassportService.flights();
  $scope.countries = PassportService.countries();
  
  $scope.tabs = [
    {title: "Home", icon: "ion-home", link: "passport.index", tabName: "index"},
    {title: "Details", icon: "ion-android-person", link: "passport.details", tabName: "details"},
    {title: "Countries", icon: "ion-flag", link: "passport.countries", tabName: "countries"},
    {title: "Map", icon: "ion-earth", link: "passport.map", tabName: "map"}
  ];
  
});