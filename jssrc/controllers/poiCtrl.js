controllerModule.controller('poiCtrl', function($scope, $state, $http, FlightService){
 
$scope.getLocation = function(id){    
    FlightService.getFlight($state.params.flightId).then(function(flight) {
      //$scope.getWeather(flight.arrivalLocation);
	  $scope.getPointsOfInterest(flight.arrivalLocation);
	  console.log(flight.arrivalLocation);
   });
  };
	
	
$scope.getPointsOfInterest = function(ln){
	var clientId = "ISTN5VSTBKGGD1SRUCRAD0RK0EM4ZSFR1GQO2C525PPOZ0GZ";
	var clientSecret = "ZNLUOAZ12MYBD4USUZT42GUW5K0DN0A2WOHT1L5WJBXC5LZJ";
	var baseURL = "https://api.foursquare.com/v2/venues/explore";
	var version = "20150101";
	var location = ln;
		
	$http.get(baseURL +"?client_id="+ clientId +"&client_secret="+ clientSecret +"&near=" + location + "&v=" + version).
      then(function(returned) {
        var jason = JSON.stringify(returned); 
        var data = JSON.parse(jason);

        $scope.pois = [];
        
        console.log(data.data.response.groups[0]);
        
        //Gets all of the nearby poi from JSON object
        var nearby = data.data.response.groups[0].items;
        for(var i = 0; i < nearby.length; i++){
          var iconpre = data.data.response.groups[0].items[i].venue.categories[0].icon.prefix;
          
          var size = "bg_64";
          var iconsuf = data.data.response.groups[0].items[i].venue.categories[0].icon.suffix;

          $scope.pois.push({ name: data.data.response.groups[0].items[i].venue.name,
                             category: data.data.response.groups[0].items[i].venue.categories[0].name,
                             icon:iconpre+size+iconsuf,
                             address: data.data.response.groups[0].items[i].venue.location.address,
                             phone: data.data.response.groups[0].items[i].venue.contact.phone,
                             rating: data.data.response.groups[0].items[i].venue.rating ,
                             note: data.data.response.groups[0].items[i].reasons.items[0].summary,
                             URL: data.data.response.groups[0].items[i].venue.url,
                             color:data.data.response.groups[0].items[i].venue.ratingColor});
        }

      }, function(response) {
        //If error...
        console.log("ERROR: " + response);
        console.log("ERROR: " + JSON.stringify(response));
      });
}
})