controllerModule.controller('NavCtrl', function($scope, $state, $ionicSideMenuDelegate, TokenService) {
  // TODO: Implement controller functionality
  $scope.links = [
    { name: 'Dashboard', link: 'home.dashboard' },
    { name: 'My Flights', link: 'flight.index' },
    { name: 'Passport', link: 'passport.index' },
    { name: 'My Calendar', link: 'calendar.calendar' },
    { name: 'Settings', link: 'settings.index' }
  ];
  
  $scope.goto = function(link) {
    $state.go(link);
  };
  
  $scope.toggleMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
  
  $scope.loggedIn = function() {
    var token = TokenService.all();
    if (token) return true;
    return false;
  } ;
  
});