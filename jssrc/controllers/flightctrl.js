/**
 * The Flight Controller is responsible for handling the flight data and updating the local storage
 * when changes to the data occur, such as new notes / checklist tasks being added to a flight
 * 
 * Injections:
 *    - $scope - The scope of the controller
 *    - $state - The ionic state manager
 *    - $ionicHistory - The ionic browsing history stack
 *    - $ionicModal - A popup modal delegate
 *    - FlightService - The service responsible for managing Flight Data at a lower level
 *    - ChecklistService - The service responsible for managing Checlist Data at a lower level
 *    - NoteService - The service responsible for managing Note Data at a lower level
 * 
 */
controllerModule.controller('FlightCtrl', function($scope, $state, $ionicModal, $ionicHistory, FlightService, ChecklistService, NoteService) {
  
  //TODO: Implement Functionality
  
  /**
   * $scope variables
   *    - $scope.flights - All flights in local storage
   *    - $scope.flightDetails - A signle flight and its details
   *    - $scope.flightId - The flight ID of the active flight (when viewing details/checklist/notes)
   *    - $scope.tabs - Used for the Tab Control at the bottom of the screen
   *    - $scope.taskModal - The modal for editing a Checklist Task Item
   *    - $scope.noteModal - The modal for editing a Note
   */
  
  $scope.flights = FlightService.all();
  
  $scope.tabs = [
    {title: "Details", icon: "ion-android-plane", link: "flight.details", tabName: "details"},
    {title: "Checklist", icon: "ion-android-checkbox-outline", link: "flight.checklist", tabName: "checklist`"},
    {title: "Notes", icon: "ion-ios-paper-outline", link: "flight.notes", tabName: "notes`"},
    {title: "POI", icon: "ion-arrow-shrink" ,link: "flight.poi", tabName: "poi"},
    {title: "Weather", icon: "ion-ios-partlysunny", link: "flight.weather", tabName: "weather"}
  ];
  
  
  // Modal Methods
  
  $ionicModal.fromTemplateUrl('./views/flight/checklist-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(taskModal) {
    $scope.taskModal = taskModal;
  });
  
  $ionicModal.fromTemplateUrl('./views/flight/note-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(noteModal) {
    $scope.noteModal = noteModal;
  });
  
  $scope.openModal = function(item, modal) {
    // Set the data
    modal.data = item;
    modal.show();
  }
  
  $scope.closeModal = function(modal) {
    modal.hide();
  }
  
  // Service Functions
  
  /**
   * Get all the particular flight associated with the selected Flight ID
   * from local storage and stores in in the $scope for reference.
   * Stores the flightId as well for reference when updating the flight
   * in local storage
   */
  FlightService.getFlight($state.params.flightId).then(function(flight) {
      $scope.flightDetails = flight;
      $scope.flightId = $state.params.flightId;
  });
  
  /**
   * Gets the notes associated with the Flight ID from local storage
   * and stores them in the $scope
   */
  NoteService.flightNotes($state.params.flightId).then(function(notes) {
      $scope.flightNotes = notes;
  });
  
  ChecklistService.flightChecklist($state.params.flightId).then(function(tasks) {
      $scope.flightChecklist = tasks;
  });
  

  /**
   * Tells the state manager to go to the flight index page. The
   * ionicHistory has a back() function, but due to a browsing
   * stack erasure issue, I have used this instead
   */  
  $scope.back = function() {
    $state.go('flight.index');
  };
  
  // Checklist Page Methods
  
  /**
   * Toggles the status of a checklist item. If the item is "done",
   * it is displayed in green. Otherwise, its color will be decided based
   * on when it becomes overdue. Saves the state of the checklist to local
   * storage
   */
  $scope.toggleChecklistItem = function(item) {
    item.done = !item.done;
    ChecklistService.updateTask(item.id, item);
  };
  
  /**
   * Creates a new checklist task and appends it to the active flight's
   * checklist array
   */
  $scope.newTask = function() {
      var task = ChecklistService.newTask($scope.flightId);
      
      $scope.flightChecklist.push(task);
      ChecklistService.append(task);
  };
  
  /**
   * Opens the Edit Task modal
   */
  $scope.editTask = function(item) {
    $scope.openModal(item, $scope.taskModal);
    
    // Items toggle when buttons are clicked. This is a
    // hack to set it back
    $scope.toggleChecklistItem(item);
  };
  
  $scope.updateTask = function(task) {
    ChecklistService.updateTask(task.id, task);
    $scope.taskModal.hide();
    
  };
  
 
  
  /**
   * Deletes the checklist task from the $scope and from
   * local storage by updating the state of the flight
   */
  $scope.deleteTask = function(itemId) {
    // Delete from $scope
    $scope.flightChecklist.forEach(function(item, i) {
		    if (item.id == itemId) {
          $scope.flightChecklist.splice(i, 1);
        }
		})
    
    // Update flight
    ChecklistService.delete(itemId);
  };
  
  
  // Note Methods
  
  /**
   * Creates a new note for the active flight and appends it
   * to the notes array
   */
  $scope.newNote = function() {
      var note = NoteService.newNote($scope.flightId);
      $scope.flightNotes.push(note);
      NoteService.append(note);
  };
  
  /**
   * Opens the Edit Note Modal
   */
  $scope.editNote = function(card) {
    $scope.openModal(card, $scope.noteModal);
  };
  
  $scope.updateNote = function(note) {
    NoteService.updateNote(note.guid, note);
    $scope.noteModal.hide();
  }
  
  /**
   * Deletes the note from the $scope and removes it from
   * local storage
   */
  $scope.deleteNote = function(noteId) {
      
      // Delete locally
      $scope.flightNotes.forEach(function(note, i) {
		      if (note.guid == noteId) {
            $scope.flightNotes.splice(i, 1);
          }
		  })
    
      // Delete from storage
      NoteService.delete(noteId);
      
      // Hide Modal
      $scope.noteModal.hide();
  };
  
  $scope.downloadFlights = function() {
    FlightService.download();
  };
  
});
