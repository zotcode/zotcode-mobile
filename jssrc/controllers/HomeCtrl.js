controllerModule.controller('HomeCtrl', function($scope, $state, ApiService, TokenService, FlightService, ChecklistService, UserService) {
  $scope.loginVM = new LoginViewModel();
  $scope.tabs = [
    {title: "Dashboard", icon: "ion-home", link: "home.dashboard", tabName: "dashboard"},
    {title: "Checklist", icon: "ion-android-checkbox-outline", link: "home.checklist", tabName: "checklist"},
  ];

  FlightService.nextFlight().then(function(flight) {
    $scope.nextFlight = flight;
    
    ChecklistService.flightChecklist($scope.nextFlight.id).then(function(tasks) {
      $scope.nextFlightTasks = tasks;
    });
  });
  
  
  $scope.$on('$ionicView.beforeEnter', function() {
    // Check for login token
    var token = TokenService.all();
    if(token) {
        // If token exists, bypass login page and display home page
        console.log("User token found. Displaying dashboard.");
        //$state.go('home.dashboard');
    } else {
        // Else, display Login page
        console.log("No token found. Displaying login page.");
        $state.go('home.login')
    } 
  });
  
  $scope.login = function() {
    console.log("Login User: " + $scope.loginVM.email + " - PW: " + $scope.loginVM.password);
    
    var params = {
      userName: $scope.loginVM.email,
      password: $scope.loginVM.password,
      grant_type: "password"
    };
    
    ApiService.post("token", params).then(function(result) {
        
        var token = result.data;
        TokenService.save(token);
        FlightService.download();
        UserService.download();
        $state.go('home.dashboard');
        $scope.nextFlight = FlightService.nextFlight();
        
    }, function(error) {
        $scope.loginVM.invalid = true;
        $scope.loginVM.errors = [ error.data.error_description ];
    });
    
  };
});